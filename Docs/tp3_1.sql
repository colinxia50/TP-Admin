/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : tp3

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-08-14 12:02:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `xy_ace`
-- ----------------------------
DROP TABLE IF EXISTS `xy_ace`;
CREATE TABLE `xy_ace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `started` varchar(255) NOT NULL COMMENT '开始服务地点 ',
  `ended` varchar(255) NOT NULL COMMENT '结束服务地点 ',
  `daytime` int(11) NOT NULL DEFAULT '1' COMMENT '需要安全车辆的天数',
  `worktime` int(11) NOT NULL COMMENT '每天工作量',
  `gocity` int(11) NOT NULL COMMENT '客户前往其他城市：0否；1是',
  `gocity_name` varchar(255) DEFAULT NULL,
  `prices` varchar(255) DEFAULT NULL COMMENT '价格',
  `place` varchar(255) DEFAULT NULL COMMENT '服务地点',
  `deleted` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_ace
-- ----------------------------
INSERT INTO `xy_ace` VALUES ('1', '2017-06-02 00:00:00', '2018-01-01 00:00:00', '杭州', '上海', '18', '12', '1', '天津', '333', '杭州', null);
INSERT INTO `xy_ace` VALUES ('2', '2020-08-20 02:03:04', '2019-07-19 01:02:03', '上海闸北', '杭州', '18', '12', '1', '杭州', '233', '上海闸北', '2016-08-14 12:00:45');

-- ----------------------------
-- Table structure for `xy_bus`
-- ----------------------------
DROP TABLE IF EXISTS `xy_bus`;
CREATE TABLE `xy_bus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ace_id` int(11) NOT NULL COMMENT '记录ID',
  `bus_type` int(11) NOT NULL COMMENT '车辆类别：1普通车；2防弹车；',
  `bus_type_num` int(11) NOT NULL COMMENT '车辆座数',
  `bus_num` int(11) NOT NULL DEFAULT '1' COMMENT '车辆数量',
  `deleted` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_bus
-- ----------------------------
INSERT INTO `xy_bus` VALUES ('1', '1', '1', '4', '1', null);
INSERT INTO `xy_bus` VALUES ('2', '1', '1', '4', '1', '2016-08-13 12:10:16');
INSERT INTO `xy_bus` VALUES ('3', '1', '1', '4', '1', '2016-08-13 12:10:15');

-- ----------------------------
-- Table structure for `xy_driver`
-- ----------------------------
DROP TABLE IF EXISTS `xy_driver`;
CREATE TABLE `xy_driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ace_id` int(11) NOT NULL COMMENT '记录ID',
  `driver_lang_type` int(11) NOT NULL COMMENT '司机语言类型：1译员司机；2普通司机',
  `driver_type` int(11) NOT NULL COMMENT '安保司机：1武装；2非武装',
  `driver_num` int(11) NOT NULL DEFAULT '1' COMMENT '司机数量',
  `deleted` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_driver
-- ----------------------------
INSERT INTO `xy_driver` VALUES ('1', '1', '2', '1', '21', null);
INSERT INTO `xy_driver` VALUES ('2', '1', '1', '1', '1', '2016-08-14 00:21:54');
INSERT INTO `xy_driver` VALUES ('3', '1', '1', '1', '1', '2016-08-14 00:21:52');
INSERT INTO `xy_driver` VALUES ('4', '1', '1', '2', '15', null);
