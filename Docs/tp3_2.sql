/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : tp3

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-08-14 14:02:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `xy_general_user`
-- ----------------------------
DROP TABLE IF EXISTS `xy_general_user`;
CREATE TABLE `xy_general_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_order` bigint(20) DEFAULT NULL,
  `g_account` text,
  `g_password` text,
  `g_realname` text,
  `g_phone` text,
  `g_email` text,
  `g_update_time` bigint(20) DEFAULT NULL,
  `g_last_login_time` bigint(20) DEFAULT NULL,
  `g_last_login_ip` text,
  `g_try_time` bigint(20) DEFAULT NULL,
  `g_status` int(11) DEFAULT NULL,
  `g_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_general_user
-- ----------------------------
INSERT INTO `xy_general_user` VALUES ('4', '2', '123456@qq.com', 'e11170b8cbd2d74102651cb967fa28e5', '214124', '21412412', '421412421@qq.com', '1471075574', '1421421421', '4324324', null, '0', null);
