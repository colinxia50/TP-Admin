/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : tp3

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-08-13 16:58:35
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `xy_access`
-- ----------------------------
DROP TABLE IF EXISTS `xy_access`;
CREATE TABLE `xy_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '等级。等级从0,1,2依次增加',
  `node_id` smallint(6) unsigned NOT NULL,
  `siteid` tinyint(4) NOT NULL DEFAULT '1' COMMENT '站点ID',
  `request_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '请求方式；0: All, 1: Get 2: Post',
  PRIMARY KEY (`role_id`,`node_id`,`siteid`),
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_access
-- ----------------------------
INSERT INTO `xy_access` VALUES ('2', '0', '1', '3', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '5', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '32', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '53', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '58', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '59', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '59', '3', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '66', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '245', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '246', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '247', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '248', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '254', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '255', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '256', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '257', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '258', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '259', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '260', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '261', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '262', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '263', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '264', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '266', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '267', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '268', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '269', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '276', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '277', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '278', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '279', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '280', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '281', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '300', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '310', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '311', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '313', '2', '0');
INSERT INTO `xy_access` VALUES ('2', '0', '314', '2', '0');

-- ----------------------------
-- Table structure for `xy_ace`
-- ----------------------------
DROP TABLE IF EXISTS `xy_ace`;
CREATE TABLE `xy_ace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `started` varchar(255) NOT NULL COMMENT '开始服务地点 ',
  `ended` varchar(255) NOT NULL COMMENT '结束服务地点 ',
  `daytime` int(11) NOT NULL DEFAULT '1' COMMENT '需要安全车辆的天数',
  `worktime` int(11) NOT NULL COMMENT '每天工作量',
  `gocity` int(11) NOT NULL COMMENT '客户前往其他城市：0否；1是',
  `gocity_name` varchar(255) DEFAULT NULL,
  `prices` varchar(255) DEFAULT NULL COMMENT '价格',
  `place` varchar(255) DEFAULT NULL COMMENT '服务地点',
  `deleted` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_ace
-- ----------------------------
INSERT INTO `xy_ace` VALUES ('1', '2016-08-08 11:34:03', '2016-08-09 11:34:08', '上海', '杭州', '1', '6', '0', null, '223', '上海', null);

-- ----------------------------
-- Table structure for `xy_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `xy_attachment`;
CREATE TABLE `xy_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '自定义标题',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '附件本身名字',
  `url` varchar(255) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '附件路径',
  `thumbs` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `size` int(10) NOT NULL DEFAULT '0' COMMENT '附件大小',
  `ext` char(10) NOT NULL DEFAULT '' COMMENT '附件扩展名',
  `width` smallint(6) DEFAULT NULL,
  `height` smallint(6) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '上传用户',
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `siteid` tinyint(4) NOT NULL DEFAULT '1' COMMENT '站点ID',
  `uploaded_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '上传时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `xy_bus`
-- ----------------------------
DROP TABLE IF EXISTS `xy_bus`;
CREATE TABLE `xy_bus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ace_id` int(11) NOT NULL COMMENT '记录ID',
  `bus_type` int(11) NOT NULL COMMENT '车辆类别：1普通车；2防弹车；',
  `bus_type_num` int(11) NOT NULL COMMENT '车辆座数',
  `bus_num` int(11) NOT NULL DEFAULT '1' COMMENT '车辆数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_bus
-- ----------------------------
INSERT INTO `xy_bus` VALUES ('1', '1', '1', '4', '1');

-- ----------------------------
-- Table structure for `xy_category`
-- ----------------------------
DROP TABLE IF EXISTS `xy_category`;
CREATE TABLE `xy_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) NOT NULL DEFAULT '0',
  `catname` varchar(30) NOT NULL DEFAULT '',
  `catdir` varchar(255) NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `child` tinyint(1) NOT NULL DEFAULT '0',
  `post_type` varchar(255) NOT NULL,
  `taxonomy` varchar(255) NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `level` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_category
-- ----------------------------
INSERT INTO `xy_category` VALUES ('1', '1', '头条', 'toutiao', '4', '0', 'news', 'category', '8', '3');
INSERT INTO `xy_category` VALUES ('2', '1', '导购', 'daogou', '4', '0', 'news', 'category', '3', '3');
INSERT INTO `xy_category` VALUES ('3', '1', '快讯', 'kuaixun', '4', '0', 'news', 'category', '5', '3');
INSERT INTO `xy_category` VALUES ('4', '1', '热点', '', '6', '1', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('6', '1', '顶级栏目', '', '0', '1', 'news', 'category', '1', '1');
INSERT INTO `xy_category` VALUES ('9', '1', '服装', '', '0', '1', 'news', 'category', '0', '1');
INSERT INTO `xy_category` VALUES ('20', '2', '测试的', '', '0', '1', 'news', 'category', '0', '1');
INSERT INTO `xy_category` VALUES ('21', '2', '5456', '', '20', '0', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('22', '2', '123', '', '20', '0', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('26', '1', '人物', 'renwu', '0', '0', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('28', '1', '艺人', 'actor', '0', '0', 'news', 'category', '0', '1');
INSERT INTO `xy_category` VALUES ('29', '1', '城市', 'city', '0', '0', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('30', '1', '男人', 'man', '9', '0', 'news', 'category', '0', '2');
INSERT INTO `xy_category` VALUES ('31', '1', '城市地产', 'csdc', '0', '1', 'news', 'category', '0', '1');
INSERT INTO `xy_category` VALUES ('32', '1', '地产人物', 'dcrw', '31', '0', 'news', 'category', '0', '2');

-- ----------------------------
-- Table structure for `xy_category_posts`
-- ----------------------------
DROP TABLE IF EXISTS `xy_category_posts`;
CREATE TABLE `xy_category_posts` (
  `term_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`term_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of xy_category_posts
-- ----------------------------
INSERT INTO `xy_category_posts` VALUES ('1', '16');
INSERT INTO `xy_category_posts` VALUES ('3', '16');

-- ----------------------------
-- Table structure for `xy_driver`
-- ----------------------------
DROP TABLE IF EXISTS `xy_driver`;
CREATE TABLE `xy_driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ace_id` int(11) NOT NULL COMMENT '记录ID',
  `driver_lang_type` int(11) NOT NULL COMMENT '司机语言类型：1译员司机；2普通司机',
  `driver_type` int(11) NOT NULL COMMENT '安保司机：1武装；2非武装',
  `driver_num` int(11) NOT NULL DEFAULT '1' COMMENT '司机数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_driver
-- ----------------------------
INSERT INTO `xy_driver` VALUES ('1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `xy_general_user`
-- ----------------------------
DROP TABLE IF EXISTS `xy_general_user`;
CREATE TABLE `xy_general_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_order` bigint(20) DEFAULT NULL,
  `g_account` text,
  `g_password` text,
  `g_realname` text,
  `g_phone` text,
  `g_email` text,
  `g_update_time` bigint(20) DEFAULT NULL,
  `g_last_login_time` bigint(20) DEFAULT NULL,
  `g_last_login_ip` text,
  `g_try_time` bigint(20) DEFAULT NULL,
  `g_status` int(11) DEFAULT NULL,
  `g_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_general_user
-- ----------------------------
INSERT INTO `xy_general_user` VALUES ('4', '2', '123456@qq.com', 'e11170b8cbd2d74102651cb967fa28e5', '214124', '21412412', '421412421@qq.com', '1471075574', '1421421421', '4324324', null, '0', null);

-- ----------------------------
-- Table structure for `xy_linkage`
-- ----------------------------
DROP TABLE IF EXISTS `xy_linkage`;
CREATE TABLE `xy_linkage` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `style` varchar(35) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `child` tinyint(1) NOT NULL DEFAULT '0',
  `arrchildid` mediumtext,
  `keyid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `setting` varchar(255) DEFAULT NULL,
  `siteid` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`keyid`),
  KEY `parentid` (`parentid`,`listorder`)
) ENGINE=InnoDB AUTO_INCREMENT=3807 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_linkage
-- ----------------------------
INSERT INTO `xy_linkage` VALUES ('3360', '城市列表', '1', '0', '0', '', '0', '1', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3361', '北京市', '0', '3780', '0', '3361', '3360', '0', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3362', '上海市', '0', '3780', '0', '3362', '3360', '0', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3363', '天津市', '0', '3780', '0', '3363', '3360', '0', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3364', '重庆市', '0', '3780', '0', '3364', '3360', '0', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3365', '河北省', '', '0', '1', '3365,3395,3396,3397,3398,3399,3400,3401,3402,3403,3404,3405', '3360', '1', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3366', '山西省', '', '0', '1', '3366,3406,3407,3408,3409,3410,3411,3412,3413,3414,3415,3416', '3360', '2', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3367', '内蒙古', '', '0', '1', '3367,3417,3418,3419,3420,3421,3422,3423,3424,3425,3426,3427,3428', '3360', '3', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3368', '辽宁省', '', '0', '1', '3368,3429,3430,3431,3432,3433,3434,3435,3436,3437,3438,3439,3440,3441,3442', '3360', '4', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3369', '吉林省', '', '0', '1', '3369,3443,3444,3445,3446,3447,3448,3449,3450,3451', '3360', '5', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3370', '黑龙江省', '', '0', '1', '3370,3452,3453,3454,3455,3456,3457,3458,3459,3460,3461,3462,3463,3464', '3360', '6', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3371', '江苏省', '', '0', '1', '3371,3465,3466,3467,3468,3469,3470,3471,3472,3473,3474,3475,3476,3477', '3360', '7', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3372', '浙江省', '', '0', '1', '3372,3478,3479,3480,3481,3482,3483,3484,3485,3486,3487,3488', '3360', '8', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3373', '安徽省', '', '0', '1', '3373,3489,3490,3491,3492,3493,3494,3495,3496,3497,3498,3499,3500,3501,3502,3503,3504,3505', '3360', '9', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3374', '福建省', '', '0', '1', '3374,3506,3507,3508,3509,3510,3511,3512,3513,3514', '3360', '10', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3375', '江西省', '', '0', '1', '3375,3515,3516,3517,3518,3519,3520,3521,3522,3523,3524,3525', '3360', '11', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3376', '山东省', '', '0', '1', '3376,3526,3527,3528,3529,3530,3531,3532,3533,3534,3535,3536,3537,3538,3539,3540,3541,3542', '3360', '12', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3377', '河南省', '', '0', '1', '3377,3543,3544,3545,3546,3547,3548,3549,3550,3551,3552,3553,3554,3555,3556,3557,3558,3559', '3360', '13', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3378', '湖北省', '', '0', '1', '3378,3560,3561,3562,3563,3564,3565,3566,3567,3568,3569,3570,3571,3572,3573,3574,3575,3576', '3360', '14', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3379', '湖南省', '', '0', '1', '3379,3577,3578,3579,3580,3581,3582,3583,3584,3585,3586,3587,3588,3589,3590', '3360', '15', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3380', '广东省', '', '0', '1', '3380,3591,3592,3593,3594,3595,3596,3597,3598,3599,3600,3601,3602,3603,3604,3605,3606,3607,3608,3609,3610,3611', '3360', '16', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3381', '广西省', '0', '0', '1', '3381,3612,3613,3614,3615,3616,3617,3618,3619,3620,3621,3622,3623,3624,3625', '3360', '17', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3382', '海南省', '', '0', '1', '3382,3626,3627,3628,3629,3630,3631,3632,3633,3634,3635,3636,3637,3638,3639,3640,3641,3642,3643,3644,3645,3646', '3360', '18', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3383', '四川省', '', '0', '1', '3383,3647,3648,3649,3650,3651,3652,3653,3654,3655,3656,3657,3658,3659,3660,3661,3662,3663,3664,3665,3666,3667', '3360', '19', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3384', '贵州省', '', '0', '1', '3384,3668,3669,3670,3671,3672,3673,3674,3675,3676', '3360', '20', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3385', '云南省', '', '0', '1', '3385,3677,3678,3679,3680,3681,3682,3683,3684,3685,3686,3687,3688,3689,3690,3691,3692', '3360', '21', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3386', '西藏', '', '0', '1', '3386,3693,3694,3695,3696,3697,3698,3699', '3360', '22', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3387', '陕西省', '', '0', '1', '3387,3700,3701,3702,3703,3704,3705,3706,3707,3708,3709', '3360', '23', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3388', '甘肃省', '', '0', '1', '3388,3710,3711,3712,3713,3714,3715,3716,3717,3718,3719,3720,3721,3722,3723', '3360', '24', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3389', '青海省', '', '0', '1', '3389,3724,3725,3726,3727,3728,3729,3730,3731', '3360', '25', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3390', '宁夏', '', '0', '1', '3390,3732,3733,3734,3735,3736', '3360', '26', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3391', '新疆', '', '0', '1', '3391,3737,3738,3739,3740,3741,3742,3743,3744,3745,3746,3747,3748,3749,3750,3751,3752,3753,3754', '3360', '27', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3392', '台湾省', '', '0', '1', '3392,3755,3756,3757,3758,3759,3760,3761,3762,3763,3764,3765,3766,3767,3768,3769,3770,3771,3772,3773,3774,3775,3776,3777,3778,3779', '3360', '28', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3393', '香港', '0', '3781', '0', '3393', '3360', '29', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3394', '澳门', '0', '3781', '0', '3394', '3360', '30', '', 'array (\n  \'level\' => \'0\',\n)', '0');
INSERT INTO `xy_linkage` VALUES ('3395', '石家庄市', '', '3365', '0', '3395', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3396', '唐山市', '', '3365', '0', '3396', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3397', '秦皇岛市', '', '3365', '0', '3397', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3398', '邯郸市', '', '3365', '0', '3398', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3399', '邢台市', '', '3365', '0', '3399', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3400', '保定市', '', '3365', '0', '3400', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3401', '张家口市', '', '3365', '0', '3401', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3402', '承德市', '', '3365', '0', '3402', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3403', '沧州市', '', '3365', '0', '3403', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3404', '廊坊市', '', '3365', '0', '3404', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3405', '衡水市', '', '3365', '0', '3405', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3406', '太原市', '', '3366', '0', '3406', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3407', '大同市', '', '3366', '0', '3407', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3408', '阳泉市', '', '3366', '0', '3408', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3409', '长治市', '', '3366', '0', '3409', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3410', '晋城市', '', '3366', '0', '3410', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3411', '朔州市', '', '3366', '0', '3411', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3412', '晋中市', '', '3366', '0', '3412', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3413', '运城市', '', '3366', '0', '3413', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3414', '忻州市', '', '3366', '0', '3414', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3415', '临汾市', '', '3366', '0', '3415', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3416', '吕梁市', '', '3366', '0', '3416', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3417', '呼和浩特市', '', '3367', '0', '3417', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3418', '包头市', '', '3367', '0', '3418', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3419', '乌海市', '', '3367', '0', '3419', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3420', '赤峰市', '', '3367', '0', '3420', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3421', '通辽市', '', '3367', '0', '3421', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3422', '鄂尔多斯市', '', '3367', '0', '3422', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3423', '呼伦贝尔市', '', '3367', '0', '3423', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3424', '巴彦淖尔市', '', '3367', '0', '3424', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3425', '乌兰察布市', '', '3367', '0', '3425', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3426', '兴安盟', '', '3367', '0', '3426', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3427', '锡林郭勒盟', '', '3367', '0', '3427', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3428', '阿拉善盟', '', '3367', '0', '3428', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3429', '沈阳市', '', '3368', '0', '3429', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3430', '大连市', '', '3368', '0', '3430', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3431', '鞍山市', '', '3368', '0', '3431', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3432', '抚顺市', '', '3368', '0', '3432', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3433', '本溪市', '', '3368', '0', '3433', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3434', '丹东市', '', '3368', '0', '3434', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3435', '锦州市', '', '3368', '0', '3435', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3436', '营口市', '', '3368', '0', '3436', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3437', '阜新市', '', '3368', '0', '3437', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3438', '辽阳市', '', '3368', '0', '3438', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3439', '盘锦市', '', '3368', '0', '3439', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3440', '铁岭市', '', '3368', '0', '3440', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3441', '朝阳市', '', '3368', '0', '3441', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3442', '葫芦岛市', '', '3368', '0', '3442', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3443', '长春市', '', '3369', '0', '3443', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3444', '吉林市', '', '3369', '0', '3444', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3445', '四平市', '', '3369', '0', '3445', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3446', '辽源市', '', '3369', '0', '3446', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3447', '通化市', '', '3369', '0', '3447', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3448', '白山市', '', '3369', '0', '3448', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3449', '松原市', '', '3369', '0', '3449', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3450', '白城市', '', '3369', '0', '3450', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3451', '延边', '', '3369', '0', '3451', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3452', '哈尔滨市', '', '3370', '0', '3452', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3453', '齐齐哈尔市', '', '3370', '0', '3453', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3454', '鸡西市', '', '3370', '0', '3454', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3455', '鹤岗市', '', '3370', '0', '3455', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3456', '双鸭山市', '', '3370', '0', '3456', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3457', '大庆市', '', '3370', '0', '3457', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3458', '伊春市', '', '3370', '0', '3458', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3459', '佳木斯市', '', '3370', '0', '3459', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3460', '七台河市', '', '3370', '0', '3460', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3461', '牡丹江市', '', '3370', '0', '3461', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3462', '黑河市', '', '3370', '0', '3462', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3463', '绥化市', '', '3370', '0', '3463', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3464', '大兴安岭地区', '', '3370', '0', '3464', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3465', '南京市', '', '3371', '0', '3465', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3466', '无锡市', '', '3371', '0', '3466', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3467', '徐州市', '', '3371', '0', '3467', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3468', '常州市', '', '3371', '0', '3468', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3469', '苏州市', '', '3371', '0', '3469', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3470', '南通市', '', '3371', '0', '3470', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3471', '连云港市', '', '3371', '0', '3471', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3472', '淮安市', '', '3371', '0', '3472', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3473', '盐城市', '', '3371', '0', '3473', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3474', '扬州市', '', '3371', '0', '3474', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3475', '镇江市', '', '3371', '0', '3475', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3476', '泰州市', '', '3371', '0', '3476', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3477', '宿迁市', '', '3371', '0', '3477', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3478', '杭州市', '', '3372', '0', '3478', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3479', '宁波市', '', '3372', '0', '3479', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3480', '温州市', '', '3372', '0', '3480', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3481', '嘉兴市', '', '3372', '0', '3481', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3482', '湖州市', '', '3372', '0', '3482', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3483', '绍兴市', '', '3372', '0', '3483', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3484', '金华市', '', '3372', '0', '3484', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3485', '衢州市', '', '3372', '0', '3485', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3486', '舟山市', '', '3372', '0', '3486', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3487', '台州市', '', '3372', '0', '3487', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3488', '丽水市', '', '3372', '0', '3488', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3489', '合肥市', '', '3373', '0', '3489', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3490', '芜湖市', '', '3373', '0', '3490', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3491', '蚌埠市', '', '3373', '0', '3491', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3492', '淮南市', '', '3373', '0', '3492', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3493', '马鞍山市', '', '3373', '0', '3493', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3494', '淮北市', '', '3373', '0', '3494', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3495', '铜陵市', '', '3373', '0', '3495', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3496', '安庆市', '', '3373', '0', '3496', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3497', '黄山市', '', '3373', '0', '3497', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3498', '滁州市', '', '3373', '0', '3498', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3499', '阜阳市', '', '3373', '0', '3499', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3500', '宿州市', '', '3373', '0', '3500', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3501', '巢湖市', '', '3373', '0', '3501', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3502', '六安市', '', '3373', '0', '3502', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3503', '亳州市', '', '3373', '0', '3503', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3504', '池州市', '', '3373', '0', '3504', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3505', '宣城市', '', '3373', '0', '3505', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3506', '福州市', '', '3374', '0', '3506', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3507', '厦门市', '', '3374', '0', '3507', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3508', '莆田市', '', '3374', '0', '3508', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3509', '三明市', '', '3374', '0', '3509', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3510', '泉州市', '', '3374', '0', '3510', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3511', '漳州市', '', '3374', '0', '3511', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3512', '南平市', '', '3374', '0', '3512', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3513', '龙岩市', '', '3374', '0', '3513', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3514', '宁德市', '', '3374', '0', '3514', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3515', '南昌市', '', '3375', '0', '3515', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3516', '景德镇市', '', '3375', '0', '3516', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3517', '萍乡市', '', '3375', '0', '3517', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3518', '九江市', '', '3375', '0', '3518', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3519', '新余市', '', '3375', '0', '3519', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3520', '鹰潭市', '', '3375', '0', '3520', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3521', '赣州市', '', '3375', '0', '3521', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3522', '吉安市', '', '3375', '0', '3522', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3523', '宜春市', '', '3375', '0', '3523', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3524', '抚州市', '', '3375', '0', '3524', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3525', '上饶市', '', '3375', '0', '3525', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3526', '济南市', '', '3376', '0', '3526', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3527', '青岛市', '', '3376', '0', '3527', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3528', '淄博市', '', '3376', '0', '3528', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3529', '枣庄市', '', '3376', '0', '3529', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3530', '东营市', '', '3376', '0', '3530', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3531', '烟台市', '', '3376', '0', '3531', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3532', '潍坊市', '', '3376', '0', '3532', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3533', '济宁市', '', '3376', '0', '3533', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3534', '泰安市', '', '3376', '0', '3534', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3535', '威海市', '', '3376', '0', '3535', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3536', '日照市', '', '3376', '0', '3536', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3537', '莱芜市', '', '3376', '0', '3537', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3538', '临沂市', '', '3376', '0', '3538', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3539', '德州市', '', '3376', '0', '3539', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3540', '聊城市', '', '3376', '0', '3540', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3541', '滨州市', '', '3376', '0', '3541', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3542', '荷泽市', '', '3376', '0', '3542', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3543', '郑州市', '', '3377', '0', '3543', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3544', '开封市', '', '3377', '0', '3544', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3545', '洛阳市', '', '3377', '0', '3545', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3546', '平顶山市', '', '3377', '0', '3546', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3547', '安阳市', '', '3377', '0', '3547', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3548', '鹤壁市', '', '3377', '0', '3548', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3549', '新乡市', '', '3377', '0', '3549', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3550', '焦作市', '', '3377', '0', '3550', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3551', '濮阳市', '', '3377', '0', '3551', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3552', '许昌市', '', '3377', '0', '3552', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3553', '漯河市', '', '3377', '0', '3553', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3554', '三门峡市', '', '3377', '0', '3554', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3555', '南阳市', '', '3377', '0', '3555', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3556', '商丘市', '', '3377', '0', '3556', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3557', '信阳市', '', '3377', '0', '3557', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3558', '周口市', '', '3377', '0', '3558', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3559', '驻马店市', '', '3377', '0', '3559', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3560', '武汉市', '', '3378', '0', '3560', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3561', '黄石市', '', '3378', '0', '3561', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3562', '十堰市', '', '3378', '0', '3562', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3563', '宜昌市', '', '3378', '0', '3563', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3564', '襄樊市', '', '3378', '0', '3564', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3565', '鄂州市', '', '3378', '0', '3565', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3566', '荆门市', '', '3378', '0', '3566', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3567', '孝感市', '', '3378', '0', '3567', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3568', '荆州市', '', '3378', '0', '3568', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3569', '黄冈市', '', '3378', '0', '3569', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3570', '咸宁市', '', '3378', '0', '3570', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3571', '随州市', '', '3378', '0', '3571', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3572', '恩施土家族苗族自治州', '', '3378', '0', '3572', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3573', '仙桃市', '', '3378', '0', '3573', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3574', '潜江市', '', '3378', '0', '3574', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3575', '天门市', '', '3378', '0', '3575', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3576', '神农架林区', '', '3378', '0', '3576', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3577', '长沙市', '', '3379', '0', '3577', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3578', '株洲市', '', '3379', '0', '3578', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3579', '湘潭市', '', '3379', '0', '3579', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3580', '衡阳市', '', '3379', '0', '3580', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3581', '邵阳市', '', '3379', '0', '3581', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3582', '岳阳市', '', '3379', '0', '3582', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3583', '常德市', '', '3379', '0', '3583', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3584', '张家界市', '', '3379', '0', '3584', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3585', '益阳市', '', '3379', '0', '3585', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3586', '郴州市', '', '3379', '0', '3586', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3587', '永州市', '', '3379', '0', '3587', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3588', '怀化市', '', '3379', '0', '3588', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3589', '娄底市', '', '3379', '0', '3589', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3590', '湘西土家族苗族自治州', '', '3379', '0', '3590', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3591', '广州市', '', '3380', '0', '3591', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3592', '韶关市', '', '3380', '0', '3592', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3593', '深圳市', '', '3380', '0', '3593', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3594', '珠海市', '', '3380', '0', '3594', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3595', '汕头市', '', '3380', '0', '3595', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3596', '佛山市', '', '3380', '0', '3596', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3597', '江门市', '', '3380', '0', '3597', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3598', '湛江市', '', '3380', '0', '3598', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3599', '茂名市', '', '3380', '0', '3599', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3600', '肇庆市', '', '3380', '0', '3600', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3601', '惠州市', '', '3380', '0', '3601', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3602', '梅州市', '', '3380', '0', '3602', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3603', '汕尾市', '', '3380', '0', '3603', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3604', '河源市', '', '3380', '0', '3604', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3605', '阳江市', '', '3380', '0', '3605', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3606', '清远市', '', '3380', '0', '3606', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3607', '东莞市', '', '3380', '0', '3607', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3608', '中山市', '', '3380', '0', '3608', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3609', '潮州市', '', '3380', '0', '3609', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3610', '揭阳市', '', '3380', '0', '3610', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3611', '云浮市', '', '3380', '0', '3611', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3612', '南宁市', '', '3381', '0', '3612', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3613', '柳州市', '', '3381', '0', '3613', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3614', '桂林市', '', '3381', '0', '3614', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3615', '梧州市', '', '3381', '0', '3615', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3616', '北海市', '', '3381', '0', '3616', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3617', '防城港市', '', '3381', '0', '3617', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3618', '钦州市', '', '3381', '0', '3618', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3619', '贵港市', '', '3381', '0', '3619', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3620', '玉林市', '', '3381', '0', '3620', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3621', '百色市', '', '3381', '0', '3621', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3622', '贺州市', '', '3381', '0', '3622', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3623', '河池市', '', '3381', '0', '3623', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3624', '来宾市', '', '3381', '0', '3624', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3625', '崇左市', '', '3381', '0', '3625', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3626', '海口市', '', '3382', '0', '3626', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3627', '三亚市', '', '3382', '0', '3627', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3628', '五指山市', '', '3382', '0', '3628', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3629', '琼海市', '', '3382', '0', '3629', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3630', '儋州市', '', '3382', '0', '3630', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3631', '文昌市', '', '3382', '0', '3631', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3632', '万宁市', '', '3382', '0', '3632', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3633', '东方市', '', '3382', '0', '3633', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3634', '定安县', '', '3382', '0', '3634', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3635', '屯昌县', '', '3382', '0', '3635', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3636', '澄迈县', '', '3382', '0', '3636', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3637', '临高县', '', '3382', '0', '3637', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3638', '白沙黎族自治县', '', '3382', '0', '3638', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3639', '昌江黎族自治县', '', '3382', '0', '3639', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3640', '乐东黎族自治县', '', '3382', '0', '3640', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3641', '陵水黎族自治县', '', '3382', '0', '3641', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3642', '保亭黎族苗族自治县', '', '3382', '0', '3642', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3643', '琼中黎族苗族自治县', '', '3382', '0', '3643', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3644', '西沙群岛', '', '3382', '0', '3644', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3645', '南沙群岛', '', '3382', '0', '3645', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3646', '中沙群岛的岛礁及其海域', '', '3382', '0', '3646', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3647', '成都市', '', '3383', '0', '3647', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3648', '自贡市', '', '3383', '0', '3648', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3649', '攀枝花市', '', '3383', '0', '3649', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3650', '泸州市', '', '3383', '0', '3650', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3651', '德阳市', '', '3383', '0', '3651', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3652', '绵阳市', '', '3383', '0', '3652', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3653', '广元市', '', '3383', '0', '3653', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3654', '遂宁市', '', '3383', '0', '3654', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3655', '内江市', '', '3383', '0', '3655', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3656', '乐山市', '', '3383', '0', '3656', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3657', '南充市', '', '3383', '0', '3657', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3658', '眉山市', '', '3383', '0', '3658', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3659', '宜宾市', '', '3383', '0', '3659', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3660', '广安市', '', '3383', '0', '3660', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3661', '达州市', '', '3383', '0', '3661', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3662', '雅安市', '', '3383', '0', '3662', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3663', '巴中市', '', '3383', '0', '3663', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3664', '资阳市', '', '3383', '0', '3664', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3665', '阿坝州', '', '3383', '0', '3665', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3666', '甘孜州', '', '3383', '0', '3666', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3667', '凉山州', '', '3383', '0', '3667', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3668', '贵阳市', '', '3384', '0', '3668', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3669', '六盘水市', '', '3384', '0', '3669', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3670', '遵义市', '', '3384', '0', '3670', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3671', '安顺市', '', '3384', '0', '3671', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3672', '铜仁地区', '', '3384', '0', '3672', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3673', '黔西南州', '', '3384', '0', '3673', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3674', '毕节地区', '', '3384', '0', '3674', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3675', '黔东南州', '', '3384', '0', '3675', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3676', '黔南州', '', '3384', '0', '3676', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3677', '昆明市', '', '3385', '0', '3677', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3678', '曲靖市', '', '3385', '0', '3678', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3679', '玉溪市', '', '3385', '0', '3679', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3680', '保山市', '', '3385', '0', '3680', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3681', '昭通市', '', '3385', '0', '3681', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3682', '丽江市', '', '3385', '0', '3682', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3683', '思茅市', '', '3385', '0', '3683', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3684', '临沧市', '', '3385', '0', '3684', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3685', '楚雄州', '', '3385', '0', '3685', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3686', '红河州', '', '3385', '0', '3686', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3687', '文山州', '', '3385', '0', '3687', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3688', '西双版纳', '', '3385', '0', '3688', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3689', '大理', '', '3385', '0', '3689', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3690', '德宏', '', '3385', '0', '3690', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3691', '怒江', '', '3385', '0', '3691', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3692', '迪庆', '', '3385', '0', '3692', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3693', '拉萨市', '', '3386', '0', '3693', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3694', '昌都', '', '3386', '0', '3694', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3695', '山南', '', '3386', '0', '3695', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3696', '日喀则', '', '3386', '0', '3696', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3697', '那曲', '', '3386', '0', '3697', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3698', '阿里', '', '3386', '0', '3698', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3699', '林芝', '', '3386', '0', '3699', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3700', '西安市', '', '3387', '0', '3700', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3701', '铜川市', '', '3387', '0', '3701', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3702', '宝鸡市', '', '3387', '0', '3702', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3703', '咸阳市', '', '3387', '0', '3703', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3704', '渭南市', '', '3387', '0', '3704', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3705', '延安市', '', '3387', '0', '3705', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3706', '汉中市', '', '3387', '0', '3706', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3707', '榆林市', '', '3387', '0', '3707', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3708', '安康市', '', '3387', '0', '3708', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3709', '商洛市', '', '3387', '0', '3709', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3710', '兰州市', '', '3388', '0', '3710', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3711', '嘉峪关市', '', '3388', '0', '3711', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3712', '金昌市', '', '3388', '0', '3712', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3713', '白银市', '', '3388', '0', '3713', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3714', '天水市', '', '3388', '0', '3714', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3715', '武威市', '', '3388', '0', '3715', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3716', '张掖市', '', '3388', '0', '3716', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3717', '平凉市', '', '3388', '0', '3717', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3718', '酒泉市', '', '3388', '0', '3718', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3719', '庆阳市', '', '3388', '0', '3719', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3720', '定西市', '', '3388', '0', '3720', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3721', '陇南市', '', '3388', '0', '3721', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3722', '临夏州', '', '3388', '0', '3722', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3723', '甘州', '', '3388', '0', '3723', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3724', '西宁市', '', '3389', '0', '3724', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3725', '海东地区', '', '3389', '0', '3725', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3726', '海州', '', '3389', '0', '3726', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3727', '黄南州', '', '3389', '0', '3727', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3728', '海南州', '', '3389', '0', '3728', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3729', '果洛州', '', '3389', '0', '3729', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3730', '玉树州', '', '3389', '0', '3730', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3731', '海西州', '', '3389', '0', '3731', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3732', '银川市', '', '3390', '0', '3732', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3733', '石嘴山市', '', '3390', '0', '3733', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3734', '吴忠市', '', '3390', '0', '3734', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3735', '固原市', '', '3390', '0', '3735', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3736', '中卫市', '', '3390', '0', '3736', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3737', '乌鲁木齐市', '', '3391', '0', '3737', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3738', '克拉玛依市', '', '3391', '0', '3738', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3739', '吐鲁番地区', '', '3391', '0', '3739', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3740', '哈密地区', '', '3391', '0', '3740', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3741', '昌吉州', '', '3391', '0', '3741', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3742', '博尔州', '', '3391', '0', '3742', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3743', '巴音郭楞州', '', '3391', '0', '3743', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3744', '阿克苏地区', '', '3391', '0', '3744', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3745', '克孜勒苏柯尔克孜自治州', '', '3391', '0', '3745', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3746', '喀什地区', '', '3391', '0', '3746', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3747', '和田地区', '', '3391', '0', '3747', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3748', '伊犁州', '', '3391', '0', '3748', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3749', '塔城地区', '', '3391', '0', '3749', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3750', '阿勒泰地区', '', '3391', '0', '3750', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3751', '石河子市', '', '3391', '0', '3751', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3752', '阿拉尔市', '', '3391', '0', '3752', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3753', '图木舒克市', '', '3391', '0', '3753', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3754', '五家渠市', '', '3391', '0', '3754', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3755', '台北市', '', '3392', '0', '3755', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3756', '高雄市', '', '3392', '0', '3756', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3757', '基隆市', '', '3392', '0', '3757', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3758', '新竹市', '', '3392', '0', '3758', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3759', '台中市', '', '3392', '0', '3759', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3760', '嘉义市', '', '3392', '0', '3760', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3761', '台南市', '', '3392', '0', '3761', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3762', '台北县', '', '3392', '0', '3762', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3763', '桃园县', '', '3392', '0', '3763', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3764', '新竹县', '', '3392', '0', '3764', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3765', '苗栗县', '', '3392', '0', '3765', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3766', '台中县', '', '3392', '0', '3766', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3767', '彰化县', '', '3392', '0', '3767', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3768', '南投县', '', '3392', '0', '3768', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3769', '云林县', '', '3392', '0', '3769', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3770', '嘉义县', '', '3392', '0', '3770', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3771', '台南县', '', '3392', '0', '3771', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3772', '高雄县', '', '3392', '0', '3772', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3773', '屏东县', '', '3392', '0', '3773', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3774', '宜兰县', '', '3392', '0', '3774', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3775', '花莲县', '', '3392', '0', '3775', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3776', '台东县', '', '3392', '0', '3776', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3777', '澎湖县', '', '3392', '0', '3777', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3778', '金门县', '', '3392', '0', '3778', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3779', '连江县', '', '3392', '0', '3779', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3780', '直辖市', '', '0', '1', '3780,3361,3362,3363,3364', '3360', '0', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3781', '特别行政区', '', '0', '1', '3781,3393,3394', '3360', '32', '', null, '0');
INSERT INTO `xy_linkage` VALUES ('3796', '日期', '', '0', '0', null, '0', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3797', '一月\r', '', '0', '1', ',1', '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3798', '二月\r', '', '0', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3799', '三月', '', '0', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3800', '1\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3801', '2\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3802', '3\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3803', '4\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3804', '5\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3805', '6\r', '', '3797', '0', null, '3796', '0', '', null, '1');
INSERT INTO `xy_linkage` VALUES ('3806', '7', '', '3797', '0', null, '3796', '0', '', null, '1');

-- ----------------------------
-- Table structure for `xy_log`
-- ----------------------------
DROP TABLE IF EXISTS `xy_log`;
CREATE TABLE `xy_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户',
  `module` char(40) NOT NULL DEFAULT '' COMMENT '模块',
  `action` varchar(20) NOT NULL DEFAULT '' COMMENT '操作',
  `querystring` varchar(255) NOT NULL DEFAULT '' COMMENT 'URL',
  `userid` smallint(5) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `ip` char(15) NOT NULL DEFAULT '' COMMENT 'IP',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：登陆失败; 1：操作成功；2：无权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1317 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_log
-- ----------------------------
INSERT INTO `xy_log` VALUES ('1001', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:06:44', '1');
INSERT INTO `xy_log` VALUES ('1002', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:06:46', '1');
INSERT INTO `xy_log` VALUES ('1003', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:06:48', '1');
INSERT INTO `xy_log` VALUES ('1004', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:06:50', '1');
INSERT INTO `xy_log` VALUES ('1005', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:06:51', '1');
INSERT INTO `xy_log` VALUES ('1006', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:06:53', '1');
INSERT INTO `xy_log` VALUES ('1007', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:06:56', '1');
INSERT INTO `xy_log` VALUES ('1008', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:07:09', '1');
INSERT INTO `xy_log` VALUES ('1009', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:07:10', '1');
INSERT INTO `xy_log` VALUES ('1010', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:07:15', '1');
INSERT INTO `xy_log` VALUES ('1011', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:07:18', '1');
INSERT INTO `xy_log` VALUES ('1012', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:07:21', '1');
INSERT INTO `xy_log` VALUES ('1013', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:07:25', '1');
INSERT INTO `xy_log` VALUES ('1014', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:07:50', '1');
INSERT INTO `xy_log` VALUES ('1015', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:09:06', '1');
INSERT INTO `xy_log` VALUES ('1016', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:09:08', '1');
INSERT INTO `xy_log` VALUES ('1017', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:09:10', '1');
INSERT INTO `xy_log` VALUES ('1018', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 13:09:15', '1');
INSERT INTO `xy_log` VALUES ('1019', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:09:17', '1');
INSERT INTO `xy_log` VALUES ('1020', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:09:19', '1');
INSERT INTO `xy_log` VALUES ('1021', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:11:50', '1');
INSERT INTO `xy_log` VALUES ('1022', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:11:52', '1');
INSERT INTO `xy_log` VALUES ('1023', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:12:53', '1');
INSERT INTO `xy_log` VALUES ('1024', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:12:55', '1');
INSERT INTO `xy_log` VALUES ('1025', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 13:13:02', '1');
INSERT INTO `xy_log` VALUES ('1026', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 13:13:17', '1');
INSERT INTO `xy_log` VALUES ('1027', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:41:07', '1');
INSERT INTO `xy_log` VALUES ('1028', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:41:09', '1');
INSERT INTO `xy_log` VALUES ('1029', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:41:14', '1');
INSERT INTO `xy_log` VALUES ('1030', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:41:41', '1');
INSERT INTO `xy_log` VALUES ('1031', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:41:44', '1');
INSERT INTO `xy_log` VALUES ('1032', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:42:38', '1');
INSERT INTO `xy_log` VALUES ('1033', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:43:37', '1');
INSERT INTO `xy_log` VALUES ('1034', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:44:13', '1');
INSERT INTO `xy_log` VALUES ('1035', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:44:18', '1');
INSERT INTO `xy_log` VALUES ('1036', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:45:17', '1');
INSERT INTO `xy_log` VALUES ('1037', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:46:22', '1');
INSERT INTO `xy_log` VALUES ('1038', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:46:41', '1');
INSERT INTO `xy_log` VALUES ('1039', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:46:45', '1');
INSERT INTO `xy_log` VALUES ('1040', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:47:23', '1');
INSERT INTO `xy_log` VALUES ('1041', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:48:53', '1');
INSERT INTO `xy_log` VALUES ('1042', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:48:56', '1');
INSERT INTO `xy_log` VALUES ('1043', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:49:23', '1');
INSERT INTO `xy_log` VALUES ('1044', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:49:25', '1');
INSERT INTO `xy_log` VALUES ('1045', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:49:37', '1');
INSERT INTO `xy_log` VALUES ('1046', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:49:40', '1');
INSERT INTO `xy_log` VALUES ('1047', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:50:35', '1');
INSERT INTO `xy_log` VALUES ('1048', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:50:40', '1');
INSERT INTO `xy_log` VALUES ('1049', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 13:51:49', '1');
INSERT INTO `xy_log` VALUES ('1050', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:51:50', '1');
INSERT INTO `xy_log` VALUES ('1051', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:52:26', '1');
INSERT INTO `xy_log` VALUES ('1052', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:52:31', '1');
INSERT INTO `xy_log` VALUES ('1053', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:52:53', '1');
INSERT INTO `xy_log` VALUES ('1054', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:52:55', '1');
INSERT INTO `xy_log` VALUES ('1055', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:53:33', '1');
INSERT INTO `xy_log` VALUES ('1056', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:53:40', '1');
INSERT INTO `xy_log` VALUES ('1057', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:54:22', '1');
INSERT INTO `xy_log` VALUES ('1058', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:54:24', '1');
INSERT INTO `xy_log` VALUES ('1059', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:54:33', '1');
INSERT INTO `xy_log` VALUES ('1060', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:55:14', '1');
INSERT INTO `xy_log` VALUES ('1061', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:55:16', '1');
INSERT INTO `xy_log` VALUES ('1062', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:55:24', '1');
INSERT INTO `xy_log` VALUES ('1063', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 13:56:55', '1');
INSERT INTO `xy_log` VALUES ('1064', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:56:56', '1');
INSERT INTO `xy_log` VALUES ('1065', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 13:57:07', '1');
INSERT INTO `xy_log` VALUES ('1066', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:01:02', '1');
INSERT INTO `xy_log` VALUES ('1067', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:01:43', '1');
INSERT INTO `xy_log` VALUES ('1068', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:02:01', '1');
INSERT INTO `xy_log` VALUES ('1069', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:02:02', '1');
INSERT INTO `xy_log` VALUES ('1070', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:02:09', '1');
INSERT INTO `xy_log` VALUES ('1071', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:02:31', '1');
INSERT INTO `xy_log` VALUES ('1072', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:02:33', '1');
INSERT INTO `xy_log` VALUES ('1073', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:02:40', '1');
INSERT INTO `xy_log` VALUES ('1074', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:03:10', '1');
INSERT INTO `xy_log` VALUES ('1075', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:03:12', '1');
INSERT INTO `xy_log` VALUES ('1076', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:03:19', '1');
INSERT INTO `xy_log` VALUES ('1077', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:08:57', '1');
INSERT INTO `xy_log` VALUES ('1078', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:08:58', '1');
INSERT INTO `xy_log` VALUES ('1079', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:09:05', '1');
INSERT INTO `xy_log` VALUES ('1080', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:12:05', '1');
INSERT INTO `xy_log` VALUES ('1081', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:12:06', '1');
INSERT INTO `xy_log` VALUES ('1082', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:12:13', '1');
INSERT INTO `xy_log` VALUES ('1083', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:12:14', '1');
INSERT INTO `xy_log` VALUES ('1084', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:12:25', '1');
INSERT INTO `xy_log` VALUES ('1085', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:12:33', '1');
INSERT INTO `xy_log` VALUES ('1086', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:12:35', '1');
INSERT INTO `xy_log` VALUES ('1087', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:29:03', '1');
INSERT INTO `xy_log` VALUES ('1088', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:17', '1');
INSERT INTO `xy_log` VALUES ('1089', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:35:18', '1');
INSERT INTO `xy_log` VALUES ('1090', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:21', '1');
INSERT INTO `xy_log` VALUES ('1091', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:35:25', '1');
INSERT INTO `xy_log` VALUES ('1092', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:27', '1');
INSERT INTO `xy_log` VALUES ('1093', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:35:29', '1');
INSERT INTO `xy_log` VALUES ('1094', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:30', '1');
INSERT INTO `xy_log` VALUES ('1095', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:35:55', '1');
INSERT INTO `xy_log` VALUES ('1096', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:56', '1');
INSERT INTO `xy_log` VALUES ('1097', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:35:57', '1');
INSERT INTO `xy_log` VALUES ('1098', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:35:59', '1');
INSERT INTO `xy_log` VALUES ('1099', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:00', '1');
INSERT INTO `xy_log` VALUES ('1100', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:01', '1');
INSERT INTO `xy_log` VALUES ('1101', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:02', '1');
INSERT INTO `xy_log` VALUES ('1102', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:03', '1');
INSERT INTO `xy_log` VALUES ('1103', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:11', '1');
INSERT INTO `xy_log` VALUES ('1104', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:12', '1');
INSERT INTO `xy_log` VALUES ('1105', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:17', '1');
INSERT INTO `xy_log` VALUES ('1106', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:22', '1');
INSERT INTO `xy_log` VALUES ('1107', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:23', '1');
INSERT INTO `xy_log` VALUES ('1108', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:23', '1');
INSERT INTO `xy_log` VALUES ('1109', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:24', '1');
INSERT INTO `xy_log` VALUES ('1110', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:29', '1');
INSERT INTO `xy_log` VALUES ('1111', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:30', '1');
INSERT INTO `xy_log` VALUES ('1112', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:30', '1');
INSERT INTO `xy_log` VALUES ('1113', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:33', '1');
INSERT INTO `xy_log` VALUES ('1114', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:36:37', '1');
INSERT INTO `xy_log` VALUES ('1115', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:36:38', '1');
INSERT INTO `xy_log` VALUES ('1116', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:37:03', '1');
INSERT INTO `xy_log` VALUES ('1117', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:37:08', '1');
INSERT INTO `xy_log` VALUES ('1118', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:37:09', '1');
INSERT INTO `xy_log` VALUES ('1119', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:39:25', '1');
INSERT INTO `xy_log` VALUES ('1120', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:39:29', '1');
INSERT INTO `xy_log` VALUES ('1121', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:39:31', '1');
INSERT INTO `xy_log` VALUES ('1122', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:39:33', '1');
INSERT INTO `xy_log` VALUES ('1123', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:39:38', '1');
INSERT INTO `xy_log` VALUES ('1124', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:39:40', '1');
INSERT INTO `xy_log` VALUES ('1125', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:39:40', '1');
INSERT INTO `xy_log` VALUES ('1126', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 14:39:41', '1');
INSERT INTO `xy_log` VALUES ('1127', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:39:59', '1');
INSERT INTO `xy_log` VALUES ('1128', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 14:40:16', '1');
INSERT INTO `xy_log` VALUES ('1129', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:40:17', '1');
INSERT INTO `xy_log` VALUES ('1130', 'admin', 'User', 'changePassword', '/TP-Admin/Public/User/changePassword.html', '1', '127.0.0.1', '2016-08-13 14:41:04', '1');
INSERT INTO `xy_log` VALUES ('1131', 'admin', 'Role', 'index', '/TP-Admin/Public/Role/index.html', '1', '127.0.0.1', '2016-08-13 14:41:07', '1');
INSERT INTO `xy_log` VALUES ('1132', 'admin', 'User', 'index', '/TP-Admin/Public/User/index.html', '1', '127.0.0.1', '2016-08-13 14:41:08', '1');
INSERT INTO `xy_log` VALUES ('1133', 'admin', 'Model', 'index', '/TP-Admin/Public/Model/index.html', '1', '127.0.0.1', '2016-08-13 14:41:10', '1');
INSERT INTO `xy_log` VALUES ('1134', 'admin', 'Post', 'index', '/TP-Admin/Public/Post/index.html', '1', '127.0.0.1', '2016-08-13 14:41:14', '1');
INSERT INTO `xy_log` VALUES ('1135', 'admin', 'Category', 'index', '/TP-Admin/Public/Category/index.html', '1', '127.0.0.1', '2016-08-13 14:41:15', '1');
INSERT INTO `xy_log` VALUES ('1136', 'admin', 'Attachment', 'album_list', '/TP-Admin/Public/Attachment/album_list.html', '1', '127.0.0.1', '2016-08-13 14:41:17', '1');
INSERT INTO `xy_log` VALUES ('1137', 'admin', 'Attachment', 'album_list', '/TP-Admin/Public/Attachment/album_list.html', '1', '127.0.0.1', '2016-08-13 14:42:34', '1');
INSERT INTO `xy_log` VALUES ('1138', 'admin', 'Attachment', 'album_list', '/TP-Admin/Public/Attachment/album_list.html', '1', '127.0.0.1', '2016-08-13 14:42:46', '1');
INSERT INTO `xy_log` VALUES ('1139', 'admin', 'Attachment', 'album_list', '/TP-Admin/Public/Attachment/album_list.html', '1', '127.0.0.1', '2016-08-13 14:42:54', '1');
INSERT INTO `xy_log` VALUES ('1140', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:45:30', '1');
INSERT INTO `xy_log` VALUES ('1141', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 14:45:37', '1');
INSERT INTO `xy_log` VALUES ('1142', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:46:07', '1');
INSERT INTO `xy_log` VALUES ('1143', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:50:14', '1');
INSERT INTO `xy_log` VALUES ('1144', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:50:41', '1');
INSERT INTO `xy_log` VALUES ('1145', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:50:53', '1');
INSERT INTO `xy_log` VALUES ('1146', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 14:51:13', '1');
INSERT INTO `xy_log` VALUES ('1147', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:52:44', '1');
INSERT INTO `xy_log` VALUES ('1148', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:52:46', '1');
INSERT INTO `xy_log` VALUES ('1149', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:52:55', '1');
INSERT INTO `xy_log` VALUES ('1150', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:52:56', '1');
INSERT INTO `xy_log` VALUES ('1151', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:53:02', '1');
INSERT INTO `xy_log` VALUES ('1152', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:53:04', '1');
INSERT INTO `xy_log` VALUES ('1153', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:53:59', '1');
INSERT INTO `xy_log` VALUES ('1154', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:54:05', '1');
INSERT INTO `xy_log` VALUES ('1155', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:55:00', '1');
INSERT INTO `xy_log` VALUES ('1156', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:55:21', '1');
INSERT INTO `xy_log` VALUES ('1157', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:55:43', '1');
INSERT INTO `xy_log` VALUES ('1158', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:55:49', '1');
INSERT INTO `xy_log` VALUES ('1159', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:56:01', '1');
INSERT INTO `xy_log` VALUES ('1160', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 14:56:17', '1');
INSERT INTO `xy_log` VALUES ('1161', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 14:57:07', '1');
INSERT INTO `xy_log` VALUES ('1162', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:57:10', '1');
INSERT INTO `xy_log` VALUES ('1163', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:57:17', '1');
INSERT INTO `xy_log` VALUES ('1164', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:58:07', '1');
INSERT INTO `xy_log` VALUES ('1165', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 14:58:21', '1');
INSERT INTO `xy_log` VALUES ('1166', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:05:12', '1');
INSERT INTO `xy_log` VALUES ('1167', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:05:32', '1');
INSERT INTO `xy_log` VALUES ('1168', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:16:47', '1');
INSERT INTO `xy_log` VALUES ('1169', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:16:50', '1');
INSERT INTO `xy_log` VALUES ('1170', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:16:50', '1');
INSERT INTO `xy_log` VALUES ('1171', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:17:03', '1');
INSERT INTO `xy_log` VALUES ('1172', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:17:30', '1');
INSERT INTO `xy_log` VALUES ('1173', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:17:32', '1');
INSERT INTO `xy_log` VALUES ('1174', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:17:41', '1');
INSERT INTO `xy_log` VALUES ('1175', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:17:56', '1');
INSERT INTO `xy_log` VALUES ('1176', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:18:01', '1');
INSERT INTO `xy_log` VALUES ('1177', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:18:30', '1');
INSERT INTO `xy_log` VALUES ('1178', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:18:34', '1');
INSERT INTO `xy_log` VALUES ('1179', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:18:50', '1');
INSERT INTO `xy_log` VALUES ('1180', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:18:55', '1');
INSERT INTO `xy_log` VALUES ('1181', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:19:30', '1');
INSERT INTO `xy_log` VALUES ('1182', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:19:35', '1');
INSERT INTO `xy_log` VALUES ('1183', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:20:46', '1');
INSERT INTO `xy_log` VALUES ('1184', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:20:51', '1');
INSERT INTO `xy_log` VALUES ('1185', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:21:50', '1');
INSERT INTO `xy_log` VALUES ('1186', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:21:52', '1');
INSERT INTO `xy_log` VALUES ('1187', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:22:02', '1');
INSERT INTO `xy_log` VALUES ('1188', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:22:48', '1');
INSERT INTO `xy_log` VALUES ('1189', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:22:57', '1');
INSERT INTO `xy_log` VALUES ('1190', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:22:59', '1');
INSERT INTO `xy_log` VALUES ('1191', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:23:14', '1');
INSERT INTO `xy_log` VALUES ('1192', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:23:19', '1');
INSERT INTO `xy_log` VALUES ('1193', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:23:20', '1');
INSERT INTO `xy_log` VALUES ('1194', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:24:52', '1');
INSERT INTO `xy_log` VALUES ('1195', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:26:32', '1');
INSERT INTO `xy_log` VALUES ('1196', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:26:54', '1');
INSERT INTO `xy_log` VALUES ('1197', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 15:27:01', '1');
INSERT INTO `xy_log` VALUES ('1198', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:27:02', '1');
INSERT INTO `xy_log` VALUES ('1199', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:30:25', '1');
INSERT INTO `xy_log` VALUES ('1200', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:31:14', '1');
INSERT INTO `xy_log` VALUES ('1201', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:31:31', '1');
INSERT INTO `xy_log` VALUES ('1202', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:31:32', '1');
INSERT INTO `xy_log` VALUES ('1203', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:31:42', '1');
INSERT INTO `xy_log` VALUES ('1204', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:31:43', '1');
INSERT INTO `xy_log` VALUES ('1205', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 15:31:57', '1');
INSERT INTO `xy_log` VALUES ('1206', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:32:01', '1');
INSERT INTO `xy_log` VALUES ('1207', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 15:32:07', '1');
INSERT INTO `xy_log` VALUES ('1208', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:32:09', '1');
INSERT INTO `xy_log` VALUES ('1209', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:32:09', '1');
INSERT INTO `xy_log` VALUES ('1210', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:32:11', '1');
INSERT INTO `xy_log` VALUES ('1211', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:32:19', '1');
INSERT INTO `xy_log` VALUES ('1212', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:32:20', '1');
INSERT INTO `xy_log` VALUES ('1213', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:32:43', '1');
INSERT INTO `xy_log` VALUES ('1214', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:33:10', '1');
INSERT INTO `xy_log` VALUES ('1215', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:33:12', '1');
INSERT INTO `xy_log` VALUES ('1216', 'admin', 'GeneralUser', 'add', '/TP-Admin/Public/GeneralUser/add.html', '1', '127.0.0.1', '2016-08-13 15:33:17', '1');
INSERT INTO `xy_log` VALUES ('1217', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:33:18', '1');
INSERT INTO `xy_log` VALUES ('1218', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 15:33:34', '1');
INSERT INTO `xy_log` VALUES ('1219', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:33:56', '1');
INSERT INTO `xy_log` VALUES ('1220', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:39:34', '1');
INSERT INTO `xy_log` VALUES ('1221', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:39:42', '1');
INSERT INTO `xy_log` VALUES ('1222', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:39:54', '1');
INSERT INTO `xy_log` VALUES ('1223', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:39:59', '1');
INSERT INTO `xy_log` VALUES ('1224', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:42:18', '1');
INSERT INTO `xy_log` VALUES ('1225', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:42:22', '1');
INSERT INTO `xy_log` VALUES ('1226', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 15:42:24', '1');
INSERT INTO `xy_log` VALUES ('1227', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 15:42:29', '1');
INSERT INTO `xy_log` VALUES ('1228', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:02:34', '1');
INSERT INTO `xy_log` VALUES ('1229', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:02:40', '1');
INSERT INTO `xy_log` VALUES ('1230', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:03:00', '1');
INSERT INTO `xy_log` VALUES ('1231', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:03:06', '1');
INSERT INTO `xy_log` VALUES ('1232', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:03:22', '1');
INSERT INTO `xy_log` VALUES ('1233', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:03:26', '1');
INSERT INTO `xy_log` VALUES ('1234', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:03:29', '1');
INSERT INTO `xy_log` VALUES ('1235', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:03:41', '1');
INSERT INTO `xy_log` VALUES ('1236', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:03:58', '1');
INSERT INTO `xy_log` VALUES ('1237', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:04:02', '1');
INSERT INTO `xy_log` VALUES ('1238', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:04:11', '1');
INSERT INTO `xy_log` VALUES ('1239', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:08:55', '1');
INSERT INTO `xy_log` VALUES ('1240', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:09:17', '1');
INSERT INTO `xy_log` VALUES ('1241', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:09:33', '1');
INSERT INTO `xy_log` VALUES ('1242', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:10:23', '1');
INSERT INTO `xy_log` VALUES ('1243', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:10:32', '1');
INSERT INTO `xy_log` VALUES ('1244', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:11:42', '1');
INSERT INTO `xy_log` VALUES ('1245', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:11:54', '1');
INSERT INTO `xy_log` VALUES ('1246', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:13:26', '1');
INSERT INTO `xy_log` VALUES ('1247', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:13:42', '1');
INSERT INTO `xy_log` VALUES ('1248', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:14:48', '1');
INSERT INTO `xy_log` VALUES ('1249', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:15:02', '1');
INSERT INTO `xy_log` VALUES ('1250', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:15:18', '1');
INSERT INTO `xy_log` VALUES ('1251', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:15:22', '1');
INSERT INTO `xy_log` VALUES ('1252', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:15:29', '1');
INSERT INTO `xy_log` VALUES ('1253', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:15:38', '1');
INSERT INTO `xy_log` VALUES ('1254', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:15:45', '1');
INSERT INTO `xy_log` VALUES ('1255', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:16:04', '1');
INSERT INTO `xy_log` VALUES ('1256', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:16:17', '1');
INSERT INTO `xy_log` VALUES ('1257', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:16:29', '1');
INSERT INTO `xy_log` VALUES ('1258', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:16:38', '1');
INSERT INTO `xy_log` VALUES ('1259', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 16:19:01', '1');
INSERT INTO `xy_log` VALUES ('1260', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:19:02', '1');
INSERT INTO `xy_log` VALUES ('1261', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:19:10', '1');
INSERT INTO `xy_log` VALUES ('1262', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:20:21', '1');
INSERT INTO `xy_log` VALUES ('1263', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:20:43', '1');
INSERT INTO `xy_log` VALUES ('1264', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 16:21:07', '1');
INSERT INTO `xy_log` VALUES ('1265', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 16:21:13', '1');
INSERT INTO `xy_log` VALUES ('1266', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:21:15', '1');
INSERT INTO `xy_log` VALUES ('1267', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:22:17', '1');
INSERT INTO `xy_log` VALUES ('1268', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:22:47', '1');
INSERT INTO `xy_log` VALUES ('1269', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:25:29', '1');
INSERT INTO `xy_log` VALUES ('1270', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:25:41', '1');
INSERT INTO `xy_log` VALUES ('1271', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:26:16', '1');
INSERT INTO `xy_log` VALUES ('1272', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:26:30', '1');
INSERT INTO `xy_log` VALUES ('1273', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 16:27:57', '1');
INSERT INTO `xy_log` VALUES ('1274', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:27:59', '1');
INSERT INTO `xy_log` VALUES ('1275', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:29:11', '1');
INSERT INTO `xy_log` VALUES ('1276', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:29:14', '1');
INSERT INTO `xy_log` VALUES ('1277', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 16:29:16', '1');
INSERT INTO `xy_log` VALUES ('1278', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:29:18', '1');
INSERT INTO `xy_log` VALUES ('1279', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:30:16', '1');
INSERT INTO `xy_log` VALUES ('1280', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:08', '1');
INSERT INTO `xy_log` VALUES ('1281', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:30', '1');
INSERT INTO `xy_log` VALUES ('1282', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:35', '1');
INSERT INTO `xy_log` VALUES ('1283', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:48', '1');
INSERT INTO `xy_log` VALUES ('1284', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:50', '1');
INSERT INTO `xy_log` VALUES ('1285', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:32:52', '1');
INSERT INTO `xy_log` VALUES ('1286', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:33:36', '1');
INSERT INTO `xy_log` VALUES ('1287', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:34:20', '1');
INSERT INTO `xy_log` VALUES ('1288', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:34:36', '1');
INSERT INTO `xy_log` VALUES ('1289', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:34:50', '1');
INSERT INTO `xy_log` VALUES ('1290', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 16:34:53', '1');
INSERT INTO `xy_log` VALUES ('1291', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:34:55', '1');
INSERT INTO `xy_log` VALUES ('1292', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:35:15', '1');
INSERT INTO `xy_log` VALUES ('1293', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:35:59', '1');
INSERT INTO `xy_log` VALUES ('1294', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:37:02', '1');
INSERT INTO `xy_log` VALUES ('1295', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:37:18', '1');
INSERT INTO `xy_log` VALUES ('1296', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:38:11', '1');
INSERT INTO `xy_log` VALUES ('1297', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:38:20', '1');
INSERT INTO `xy_log` VALUES ('1298', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:38:23', '1');
INSERT INTO `xy_log` VALUES ('1299', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:40:29', '1');
INSERT INTO `xy_log` VALUES ('1300', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:40:31', '1');
INSERT INTO `xy_log` VALUES ('1301', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:40:50', '1');
INSERT INTO `xy_log` VALUES ('1302', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:41:14', '1');
INSERT INTO `xy_log` VALUES ('1303', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:41:20', '1');
INSERT INTO `xy_log` VALUES ('1304', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:41:49', '1');
INSERT INTO `xy_log` VALUES ('1305', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:42:17', '1');
INSERT INTO `xy_log` VALUES ('1306', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:42:23', '1');
INSERT INTO `xy_log` VALUES ('1307', 'admin', 'GeneralUser', 'search', '/TP-Admin/Public/GeneralUser/search.html', '1', '127.0.0.1', '2016-08-13 16:42:26', '1');
INSERT INTO `xy_log` VALUES ('1308', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:45:39', '1');
INSERT INTO `xy_log` VALUES ('1309', 'admin', 'Index', 'cache_clean', '/TP-Admin/Public/Index/cache_clean.html', '1', '127.0.0.1', '2016-08-13 16:45:41', '1');
INSERT INTO `xy_log` VALUES ('1310', 'admin', 'Role', 'index', '/TP-Admin/Public/Role/index.html', '1', '127.0.0.1', '2016-08-13 16:56:51', '1');
INSERT INTO `xy_log` VALUES ('1311', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:56:54', '1');
INSERT INTO `xy_log` VALUES ('1312', 'admin', 'GeneralUser', 'edit', '/TP-Admin/Public/GeneralUser/edit.html', '1', '127.0.0.1', '2016-08-13 16:56:57', '1');
INSERT INTO `xy_log` VALUES ('1313', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:56:59', '1');
INSERT INTO `xy_log` VALUES ('1314', 'admin', 'GeneralUser', 'del', '/TP-Admin/Public/GeneralUser/del.html', '1', '127.0.0.1', '2016-08-13 16:57:01', '1');
INSERT INTO `xy_log` VALUES ('1315', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:57:03', '1');
INSERT INTO `xy_log` VALUES ('1316', 'admin', 'GeneralUser', 'index', '/TP-Admin/Public/GeneralUser/index.html', '1', '127.0.0.1', '2016-08-13 16:57:03', '1');

-- ----------------------------
-- Table structure for `xy_model`
-- ----------------------------
DROP TABLE IF EXISTS `xy_model`;
CREATE TABLE `xy_model` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `tablename` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `listorder` smallint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_model
-- ----------------------------
INSERT INTO `xy_model` VALUES ('27', '1', '资讯', 'news', '', '0', '0');

-- ----------------------------
-- Table structure for `xy_model_field`
-- ----------------------------
DROP TABLE IF EXISTS `xy_model_field`;
CREATE TABLE `xy_model_field` (
  `fieldid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `modelid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `siteid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` text,
  `css` varchar(30) NOT NULL DEFAULT '',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `errortips` varchar(255) NOT NULL DEFAULT '',
  `formtype` varchar(20) NOT NULL DEFAULT '',
  `setting` mediumtext,
  `formattribute` varchar(255) NOT NULL DEFAULT '',
  `unsetgroupids` varchar(255) NOT NULL DEFAULT '',
  `unsetroleids` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isunique` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isbase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isadd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `islist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isfulltext` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isposition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isomnipotent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fieldid`),
  KEY `modelid` (`modelid`,`disabled`),
  KEY `field` (`field`,`modelid`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_model_field
-- ----------------------------
INSERT INTO `xy_model_field` VALUES ('203', '27', '1', 'title', '标题', '', 'inputtitle', '1', '80', '', '请输入标题', 'title', '', '', '', '', '0', '1', '0', '1', '1', '1', '1', '1', '1', '4', '0', '0');
INSERT INTO `xy_model_field` VALUES ('204', '27', '1', 'updatetime', '更新时间', '', '', '0', '0', '', '', 'datetime', 'array (\r\n  \'dateformat\' => \'datetime\',\r\n  \'format\' => \'Y-m-d H:i:s\',\r\n  \'defaulttype\' => \'0\',\r\n  \'defaultvalue\' => \'\',\r\n)', '', '', '', '1', '1', '0', '1', '0', '0', '0', '0', '0', '12', '0', '0');
INSERT INTO `xy_model_field` VALUES ('205', '27', '1', 'inputtime', '发布时间', '', '', '0', '0', '', '', 'datetime', 'array (\n  \'fieldtype\' => \'datetime\',\n  \'format\' => \'Y-m-d H:i:s\',\n  \'defaulttype\' => \'0\',\n)', '', '', '', '0', '1', '0', '0', '0', '0', '0', '0', '1', '17', '0', '0');
INSERT INTO `xy_model_field` VALUES ('206', '27', '1', 'listorder', '排序', '', '', '0', '6', '', '', 'number', '', '', '', '', '1', '1', '0', '1', '0', '0', '0', '0', '0', '51', '0', '0');
INSERT INTO `xy_model_field` VALUES ('207', '27', '1', 'text', '单行文本', '', '', '0', '0', '', '', 'text', 'array (\n  \'size\' => \'50\',\n  \'defaultvalue\' => \'\',\n  \'ispassword\' => \'0\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('208', '27', '1', 'textarea', '多行文本', '', '', '0', '0', '', '', 'textarea', 'array (\n  \'width\' => \'98\',\n  \'height\' => \'46\',\n  \'defaultvalue\' => \'\',\n  \'enablehtml\' => \'0\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('209', '27', '1', 'editor', '编辑器', '', '', '0', '0', '', '', 'editor', 'array (\n  \'toolbar\' => \'basic\',\n  \'defaultvalue\' => \'\',\n  \'enablekeylink\' => \'0\',\n  \'replacenum\' => \'1\',\n  \'link_mode\' => \'0\',\n  \'enablesaveimage\' => \'0\',\n  \'height\' => \'200\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('210', '27', '1', 'select', '选线', '', '', '0', '0', '', '', 'box', 'array (\n  \'options\' => \'选项名称1|选项值1\r\n选项名称2|选项值2\r\n选项名称3|选项值3\r\n选项名称4|选项值4\',\n  \'boxtype\' => \'select\',\n  \'fieldtype\' => \'varchar\',\n  \'minnumber\' => \'1\',\n  \'width\' => \'80\',\n  \'size\' => \'1\',\n  \'defaultvalue\' => \'\',\n  \'outputtype\' => \'0\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('211', '27', '1', 'image', '图片', '', '', '0', '0', '', '', 'image', 'array (\n  \'size\' => \'\',\n  \'defaultvalue\' => \'\',\n  \'show_type\' => \'0\',\n  \'upload_allowext\' => \'gif|jpg|jpeg|png|bmp\',\n  \'watermark\' => \'0\',\n  \'isselectimage\' => \'1\',\n  \'images_width\' => \'\',\n  \'images_height\' => \'\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('212', '27', '1', 'mutile_image', '多图上传', '', '', '0', '0', '', '', 'images', 'array (\n  \'upload_allowext\' => \'gif|jpg|jpeg|png|bmp\',\n  \'isselectimage\' => \'0\',\n  \'upload_number\' => \'10\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('213', '27', '1', 'date', '日期', '', '', '0', '0', '', '', 'datetime', 'array (\n  \'fieldtype\' => \'date\',\n  \'format\' => \'Y-m-d Ah:i:s\',\n  \'defaulttype\' => \'0\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('214', '27', '1', 'linkage', '联动菜单', '', '', '0', '0', '', '', 'linkage', 'array (\n  \'linkageid\' => \'3360\',\n)', '', '', '', '0', '1', '0', '1', '0', '1', '0', '0', '0', '5', '0', '0');
INSERT INTO `xy_model_field` VALUES ('215', '28', '2', 'title', '标题', '', 'inputtitle', '1', '80', '', '请输入标题', 'title', '', '', '', '', '0', '1', '0', '1', '1', '1', '1', '1', '1', '4', '0', '0');
INSERT INTO `xy_model_field` VALUES ('216', '28', '2', 'updatetime', '更新时间', '', '', '0', '0', '', '', 'datetime', 'array (\r\n  \'dateformat\' => \'datetime\',\r\n  \'format\' => \'Y-m-d H:i:s\',\r\n  \'defaulttype\' => \'0\',\r\n  \'defaultvalue\' => \'\',\r\n)', '', '', '', '1', '1', '0', '1', '0', '0', '0', '0', '0', '12', '0', '0');
INSERT INTO `xy_model_field` VALUES ('217', '28', '2', 'inputtime', '发布时间', '', '', '0', '0', '', '', 'datetime', 'array (\n  \'fieldtype\' => \'datetime\',\n  \'format\' => \'Y-m-d H:i:s\',\n  \'defaulttype\' => \'0\',\n)', '', '', '', '0', '1', '0', '0', '0', '0', '0', '0', '1', '17', '0', '0');
INSERT INTO `xy_model_field` VALUES ('218', '28', '2', 'listorder', '排序', '', '', '0', '6', '', '', 'number', '', '', '', '', '1', '1', '0', '1', '0', '0', '0', '0', '0', '51', '0', '0');

-- ----------------------------
-- Table structure for `xy_news`
-- ----------------------------
DROP TABLE IF EXISTS `xy_news`;
CREATE TABLE `xy_news` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) NOT NULL,
  `title` varchar(255) NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `inputtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `text` varchar(255) NOT NULL DEFAULT '',
  `textarea` mediumtext NOT NULL,
  `editor` mediumtext NOT NULL,
  `select` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `mutile_image` mediumtext NOT NULL,
  `date` date DEFAULT NULL,
  `linkage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `listorder` (`listorder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_news
-- ----------------------------

-- ----------------------------
-- Table structure for `xy_node`
-- ----------------------------
DROP TABLE IF EXISTS `xy_node`;
CREATE TABLE `xy_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `post_type` varchar(255) NOT NULL DEFAULT '',
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `params` varchar(255) NOT NULL DEFAULT '',
  `request_method` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证请求方式；0: 否,1:是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_node
-- ----------------------------
INSERT INTO `xy_node` VALUES ('3', 'Menu', 'index', '菜单管理', '1', '', '0', '69', '', '0');
INSERT INTO `xy_node` VALUES ('5', 'Index', 'main', '默认首页', '1', '', '0', '58', '', '0');
INSERT INTO `xy_node` VALUES ('11', 'Menu', 'add', '添加', '1', '', '0', '3', '', '1');
INSERT INTO `xy_node` VALUES ('12', 'Menu', 'edit', '编辑', '1', '', '0', '3', '', '1');
INSERT INTO `xy_node` VALUES ('14', 'Menu', 'del', '删除', '1', '', '0', '3', '', '0');
INSERT INTO `xy_node` VALUES ('16', 'Role', 'index', '角色管理', '1', '', '0', '68', '', '0');
INSERT INTO `xy_node` VALUES ('17', 'Role', 'edit', '编辑', '1', '', '0', '16', '', '1');
INSERT INTO `xy_node` VALUES ('19', 'User', 'index', '管理员管理', '1', '', '0', '68', '', '0');
INSERT INTO `xy_node` VALUES ('21', 'User', 'add', '添加', '1', '', '0', '19', '', '1');
INSERT INTO `xy_node` VALUES ('22', 'User', 'edit', '编辑', '1', '', '0', '19', '', '1');
INSERT INTO `xy_node` VALUES ('23', 'User', 'del', '删除', '1', '', '0', '19', '', '0');
INSERT INTO `xy_node` VALUES ('25', 'Log', 'index', '操作日志', '1', '', '0', '69', '', '0');
INSERT INTO `xy_node` VALUES ('32', 'User', 'changePassword', '密码修改', '1', '', '0', '58', '', '1');
INSERT INTO `xy_node` VALUES ('41', 'Log', 'search', '查询', '1', '', '0', '25', '', '0');
INSERT INTO `xy_node` VALUES ('53', 'Index', 'cache_clean', '缓存清理', '1', '', '0', '66', '', '0');
INSERT INTO `xy_node` VALUES ('58', 'Index', 'left', '个人信息', '1', '', '0', '59', '', '0');
INSERT INTO `xy_node` VALUES ('59', 'Index', 'index', '我的面板', '1', '', '10', '0', '', '0');
INSERT INTO `xy_node` VALUES ('62', 'Role', 'index', '设置', '1', '', '9', '0', '', '0');
INSERT INTO `xy_node` VALUES ('63', 'Menu', 'index', '扩展', '1', '', '5', '0', '', '0');
INSERT INTO `xy_node` VALUES ('66', 'Convenience', 'index', '快捷操作', '1', '', '0', '59', '', '0');
INSERT INTO `xy_node` VALUES ('68', 'Manager', 'index', '管理员设置', '1', '', '0', '62', '', '0');
INSERT INTO `xy_node` VALUES ('69', 'Menu', 'index', '扩展管理', '1', '', '0', '63', '', '0');
INSERT INTO `xy_node` VALUES ('114', 'Site', 'index', '相关设置', '1', '', '1', '62', '', '0');
INSERT INTO `xy_node` VALUES ('115', 'Site', 'index', '站点管理', '1', '', '0', '114', '', '0');
INSERT INTO `xy_node` VALUES ('116', 'Site', 'add', '添加', '1', '', '0', '115', '', '1');
INSERT INTO `xy_node` VALUES ('117', 'Site', 'edit', '修改', '1', '', '0', '115', '', '1');
INSERT INTO `xy_node` VALUES ('245', 'Post', 'index', '内容', '1', '', '8', '0', '', '0');
INSERT INTO `xy_node` VALUES ('248', 'Category', 'index', '栏目管理', '1', '', '3', '316', '', '0');
INSERT INTO `xy_node` VALUES ('249', 'Model', 'index', '模型管理', '1', '', '2', '315', '', '0');
INSERT INTO `xy_node` VALUES ('253', 'Linkage', 'index', '联动菜单', '1', '', '0', '69', '', '0');
INSERT INTO `xy_node` VALUES ('254', 'Post', 'add', '添加', '1', '', '0', '325', '', '1');
INSERT INTO `xy_node` VALUES ('255', 'Post', 'edit', '修改', '1', '', '0', '325', '', '1');
INSERT INTO `xy_node` VALUES ('256', 'Post', 'delete', '删除', '1', '', '0', '325', '', '0');
INSERT INTO `xy_node` VALUES ('263', 'Post', 'public_check_title', '标题重复检测', '1', '', '0', '325', '', '0');
INSERT INTO `xy_node` VALUES ('267', 'User', 'public_checkname_ajx', '异步检测用户名', '1', '', '0', '32', '', '0');
INSERT INTO `xy_node` VALUES ('268', 'User', 'public_password_ajx', '异步检测密码', '1', '', '0', '32', '', '0');
INSERT INTO `xy_node` VALUES ('269', 'User', 'public_email_ajx', '异步检测emial合法性', '1', '', '0', '32', '', '0');
INSERT INTO `xy_node` VALUES ('270', 'Site', 'delete', '删除', '1', '', '0', '115', '', '0');
INSERT INTO `xy_node` VALUES ('271', 'Site', 'public_name', '站点名称重复检测', '1', '', '0', '115', '', '0');
INSERT INTO `xy_node` VALUES ('272', 'Site', 'public_dirname', '站点目录重名检测', '1', '', '0', '115', '', '0');
INSERT INTO `xy_node` VALUES ('273', 'Role', 'add', '添加', '1', '', '0', '16', '', '1');
INSERT INTO `xy_node` VALUES ('274', 'Role', 'del', '删除', '1', '', '0', '16', '', '0');
INSERT INTO `xy_node` VALUES ('275', 'Role', 'limits', '授权', '1', '', '0', '16', '', '1');
INSERT INTO `xy_node` VALUES ('277', 'Category', 'add', '添加', '1', '', '0', '248', '', '1');
INSERT INTO `xy_node` VALUES ('278', 'Category', 'edit', '修改', '1', '', '0', '248', '', '1');
INSERT INTO `xy_node` VALUES ('279', 'Category', 'del', '删除', '1', '', '0', '248', '', '0');
INSERT INTO `xy_node` VALUES ('280', 'Category', 'listorder', '排序', '1', '', '0', '248', '', '0');
INSERT INTO `xy_node` VALUES ('281', 'Category', 'public_check_catdir', 'ajax检查栏目是否存在', '1', '', '0', '248', '', '0');
INSERT INTO `xy_node` VALUES ('282', 'Model', 'add', '添加', '1', '', '0', '249', '', '1');
INSERT INTO `xy_node` VALUES ('283', 'Model', 'edit', '编辑', '1', '', '0', '249', '', '1');
INSERT INTO `xy_node` VALUES ('284', 'Model', 'delete', '删除', '1', '', '0', '249', '', '0');
INSERT INTO `xy_node` VALUES ('285', 'Model', 'public_check_name', '模型名称重复异步检测', '1', '', '0', '249', '', '0');
INSERT INTO `xy_node` VALUES ('292', 'Menu', 'listorder', '排序', '1', '', '0', '3', '', '0');
INSERT INTO `xy_node` VALUES ('293', 'Linkage', 'show_sub_menu', '显示子菜单', '1', '', '0', '253', '', '0');
INSERT INTO `xy_node` VALUES ('294', 'Linkage', 'add', '添加子菜单', '1', '', '0', '253', '', '1');
INSERT INTO `xy_node` VALUES ('295', 'Linkage', 'add_top_menu', '添加顶级菜单', '1', '', '0', '253', '', '0');
INSERT INTO `xy_node` VALUES ('296', 'Linkage', 'edit', '编辑', '1', '', '0', '253', '', '1');
INSERT INTO `xy_node` VALUES ('297', 'Linkage', 'listorder', '排序', '1', '', '0', '253', '', '0');
INSERT INTO `xy_node` VALUES ('298', 'Linkage', 'delete', '删除子菜单', '1', '', '0', '253', '', '0');
INSERT INTO `xy_node` VALUES ('299', 'Linkage', 'delete_top', '删除顶级菜单', '1', '', '0', '253', '', '0');
INSERT INTO `xy_node` VALUES ('301', 'ModelField', 'index', '模型字段管理', '1', '', '0', '249', '', '0');
INSERT INTO `xy_node` VALUES ('302', 'ModelField', 'add', '添加字段', '1', '', '0', '301', '', '1');
INSERT INTO `xy_node` VALUES ('303', 'ModelField', 'edit', '修改字段', '1', '', '0', '301', '', '1');
INSERT INTO `xy_node` VALUES ('304', 'ModelField', 'delete', '删除字段', '1', '', '0', '301', '', '0');
INSERT INTO `xy_node` VALUES ('305', 'ModelField', 'disabled', '禁用字段', '1', '', '0', '301', '', '0');
INSERT INTO `xy_node` VALUES ('306', 'ModelField', 'listorder', '字段排序', '1', '', '0', '301', '', '0');
INSERT INTO `xy_node` VALUES ('307', 'ModelField', 'public_checkfield', '字段重复检查', '1', '', '0', '301', '', '0');
INSERT INTO `xy_node` VALUES ('308', 'ModelField', 'public_field_setting', '字段属性异步载入', '1', '', '0', '301', '', '0');
INSERT INTO `xy_node` VALUES ('309', 'ModelField', 'public_priview', '模型预览', '1', '', '1', '249', '', '0');
INSERT INTO `xy_node` VALUES ('310', 'Attachment', 'index', '附件管理', '1', '', '1', '245', '', '0');
INSERT INTO `xy_node` VALUES ('311', 'Attachment', 'album_list', '图库', '1', '', '0', '310', '', '0');
INSERT INTO `xy_node` VALUES ('312', 'Index', 'change_site', '站点切换', '1', '', '0', '115', '', '0');
INSERT INTO `xy_node` VALUES ('315', 'Module', 'index', '内容设置', '1', '', '0', '62', '', '0');
INSERT INTO `xy_node` VALUES ('316', 'Taxonomy', 'index', '分类管理', '1', '', '0', '315', '', '0');
INSERT INTO `xy_node` VALUES ('317', 'Taxonomy', 'register', '注册分类', '1', '', '0', '316', '', '1');
INSERT INTO `xy_node` VALUES ('318', 'Taxonomy', 'delete', '注销分类', '1', '', '0', '316', '', '0');
INSERT INTO `xy_node` VALUES ('320', 'Post', 'index', 'TP4', '1', '', '0', '319', 'moduleid=25', '0');
INSERT INTO `xy_node` VALUES ('323', 'Post', 'index', '内容', '1', '', '0', '322', 'moduleid=16', '0');
INSERT INTO `xy_node` VALUES ('325', 'Post', 'index', '模型内容管理', '1', '', '0', '249', '', '0');
INSERT INTO `xy_node` VALUES ('326', 'Post', 'listorder', '排序', '1', '', '0', '325', '', '0');
INSERT INTO `xy_node` VALUES ('327', 'Post', 'index', '资讯管理', '1', 'news', '0', '245', '', '0');
INSERT INTO `xy_node` VALUES ('328', 'Post', 'index', '资讯', '1', '', '0', '327', 'moduleid=27', '0');
INSERT INTO `xy_node` VALUES ('329', 'Category', 'index', '栏目', '1', '', '0', '327', 'post_type=news&taxonomy_name=category', '0');
INSERT INTO `xy_node` VALUES ('330', 'Post', 'index', '内容模型管理', '1', 'fc_news', '0', '245', '', '0');
INSERT INTO `xy_node` VALUES ('331', 'Post', 'index', '内容模型', '1', '', '0', '330', 'moduleid=28', '0');
INSERT INTO `xy_node` VALUES ('336', 'Ace', 'index', '安全车辆', '1', '', '0', '315', '', '0');
INSERT INTO `xy_node` VALUES ('337', 'GeneralUser', 'index', '会员', '1', '', '0', '0', '', '0');
INSERT INTO `xy_node` VALUES ('338', 'GeneralUser', 'index', '会员管理', '1', '', '0', '337', '', '0');
INSERT INTO `xy_node` VALUES ('339', 'GeneralUser', 'index', '普通会员', '1', '', '0', '338', 'moduleid=29', '0');

-- ----------------------------
-- Table structure for `xy_role`
-- ----------------------------
DROP TABLE IF EXISTS `xy_role`;
CREATE TABLE `xy_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_role
-- ----------------------------
INSERT INTO `xy_role` VALUES ('1', '超级管理员', null, '1', '超级管理员');
INSERT INTO `xy_role` VALUES ('2', '管理员', null, '1', '管理员');

-- ----------------------------
-- Table structure for `xy_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `xy_role_user`;
CREATE TABLE `xy_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_role_user
-- ----------------------------
INSERT INTO `xy_role_user` VALUES ('1', '1');
INSERT INTO `xy_role_user` VALUES ('2', '2');
INSERT INTO `xy_role_user` VALUES ('2', '37');
INSERT INTO `xy_role_user` VALUES ('2', '39');
INSERT INTO `xy_role_user` VALUES ('2', '42');
INSERT INTO `xy_role_user` VALUES ('2', '43');
INSERT INTO `xy_role_user` VALUES ('2', '44');
INSERT INTO `xy_role_user` VALUES ('2', '45');
INSERT INTO `xy_role_user` VALUES ('2', '46');
INSERT INTO `xy_role_user` VALUES ('2', '47');
INSERT INTO `xy_role_user` VALUES ('2', '48');
INSERT INTO `xy_role_user` VALUES ('2', '49');
INSERT INTO `xy_role_user` VALUES ('2', '50');
INSERT INTO `xy_role_user` VALUES ('2', '48');
INSERT INTO `xy_role_user` VALUES ('19', '49');
INSERT INTO `xy_role_user` VALUES ('2', '49');
INSERT INTO `xy_role_user` VALUES ('2', '50');
INSERT INTO `xy_role_user` VALUES ('2', '51');

-- ----------------------------
-- Table structure for `xy_site`
-- ----------------------------
DROP TABLE IF EXISTS `xy_site`;
CREATE TABLE `xy_site` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) DEFAULT '',
  `dirname` char(255) DEFAULT '',
  `domain` char(255) DEFAULT '',
  `site_title` char(255) DEFAULT '',
  `keywords` char(255) DEFAULT '',
  `description` char(255) DEFAULT '',
  `release_point` text,
  `default_style` char(50) DEFAULT NULL,
  `template` text,
  `setting` mediumtext,
  `uuid` char(40) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_site
-- ----------------------------
INSERT INTO `xy_site` VALUES ('1', '红海螺', 'www', 'http://www.hhailuo.com', 'XiaoYaoCMS', 'XiaoYaoCMS 房地产CMS 微信公众平台', 'XiaoYaoCMS 房产门户系统，集成微信公众平台', '', 'default', 'Admin', 'array (\n  \'upload_maxsize\' => \'2048\',\n  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',\n  \'watermark_enable\' => \'0\',\n  \'watermark_minwidth\' => \'300\',\n  \'watermark_minheight\' => \'300\',\n  \'watermark_img\' => \'/mark.png\',\n  \'watermark_pct\' => \'85\',\n  \'watermark_quality\' => \'80\',\n)', '3d3fd04a-6433-11e1-b487-1c750850a1c3');
INSERT INTO `xy_site` VALUES ('2', '红海螺-房产', 'house', 'http://www.house.hhailuo.com', '红海螺-房产', '', '', null, null, null, 'array (\n  \'upload_maxsize\' => \'2048\',\n  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',\n  \'watermark_enable\' => \'0\',\n  \'watermark_minwidth\' => \'300\',\n  \'watermark_minheight\' => \'300\',\n  \'watermark_img\' => \'mark.gif\',\n  \'watermark_pct\' => \'100\',\n  \'watermark_quality\' => \'80\',\n)', '');
INSERT INTO `xy_site` VALUES ('3', 'xiaoyao', 'xiaoyao', 'http://tp-xiaoyao.hhailuo.com', 'xiaoyao', 'xiaoyao', 'xiaoyao', null, null, null, 'array (\n  \'upload_maxsize\' => \'2048\',\n  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',\n  \'watermark_enable\' => \'0\',\n  \'watermark_minwidth\' => \'300\',\n  \'watermark_minheight\' => \'300\',\n  \'watermark_img\' => \'mark.gif\',\n  \'watermark_pct\' => \'100\',\n  \'watermark_quality\' => \'80\',\n)', '');

-- ----------------------------
-- Table structure for `xy_user`
-- ----------------------------
DROP TABLE IF EXISTS `xy_user`;
CREATE TABLE `xy_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `realname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `try_time` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '错误次数',
  `status` tinyint(1) DEFAULT '0',
  `role_id` tinyint(2) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xy_user
-- ----------------------------
INSERT INTO `xy_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '1471022290', null, '0', '1', '1');
INSERT INTO `xy_user` VALUES ('45', 'tp-admin', 'b990e224fe46ed9cf0a8cd8c49d59629', '', '', '1453702321', null, '0', '1', '2');
