<?php
/**
 * Created by PhpStorm.
 * User: C2
 * Date: 2016/8/10
 * Time: 16:11
 */

namespace Admin\Model;
use Think\Model;

class AceModel extends Model
{
    public function aceList()
    {
        $option['where']['deleted'] = array('exp','is null');
        $option['field'] = array('id','starttime','endtime','started','ended','daytime','worktime','gocity','gocity_name','prices','place');
        $result = $this->select($option);
        return $result;
    }
    public function getAce($id)
    {
        if(!empty($id))
        {
            $option['where']['id'] = array('eq',$id);
        }
        $option['where']['deleted'] = array('exp','is null');
        $option['field'] = array('id','starttime','endtime','started','ended','daytime','worktime','gocity','gocity_name','prices','place');
        $result = $this->find($option);
        return $result;
    }
}