<?php

namespace Admin\Model;

use Think\Model;

class GeneralUserModel extends Model {
	public function get_general_info() {
		$result = $this->field ( 'id,g_order,g_account,g_email,g_phone,g_update_time' )->order ( 'g_order asc' )->select ();
		return $result;
	}
	public function get_save_data($postdata) {
		$data = array ();
		foreach ( $postdata as $key => $x ) {
			if ($key == "re_pwd") {
				continue;
			} elseif ($key == "pwd") {
				$data ['g_password'] = md5 ( trim ( $postdata [$x] ) );
			} else {
				$data [$key] = $x;
			}
		}
		$data ['g_update_time'] = time ();
		return $data;
	}
	public function search_info() {
		$data ['start_time'] = strtotime ( $_POST ['start_time'] );
		$data ['end_time'] = strtotime ( $_POST ['end_time'] );
		$data ['g_name'] = "%" . $_POST ['generalname'] . "%";
		if (($_POST ['start_time']) && ($_POST ['end_time'])) {
			$map ['g_update_time'] = array (
					'between',
					array (
							$data ['start_time'],
							$data ['end_time'] 
					) 
			);
			$map ['g_account'] = array (
					'like',
					$data ['g_name'],
					'AND' 
			);
			$search_info = $this->where ( $map )->order ( 'g_order asc' )->select ();
			return $search_info;
		} else {
			$map ['g_account'] = array (
					'like',
					$data ['g_name'] 
			);
			$search_info = $this->where ( $map )->order ( 'g_order asc' )->select ();
			return $search_info;
		}
	}
}