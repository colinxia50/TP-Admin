<?php
// +----------------------------------------------------------------------
// | TP-Admin [ 多功能后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2016 http://www.hhailuo.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 逍遥·李志亮 <xiaoyao.working@gmail.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Controller\CommonController;

/**
 * 安全车辆
 */
class AceController extends CommonController {
    public $AceModel;
    public $busModel;
    public function _initialize()
    {
        $this->AceModel = D("Ace");
        $this->busModel = D("bus");
        $this->driverModel = D("driver");
    }

    public function index()
    {
        $rs = $this->AceModel->aceList();
        $this->assign('rs',$rs);
        $this->display();
    }
    public function details()
    {
        $id = I('id');
        $rs = $this->AceModel->getAce($id);
        $option['where']['ace_id'] = array('eq',$id);
        $option['where']['deleted'] = array('exp','is null');
        $bus = $this->busModel->select($option);
        $driver = $this->driverModel->select($option);
        $this->assign('rs',$rs);
        $this->assign('bus',$bus);
        $this->assign('driver',$driver);
        $this->display();
    }
    public function adds()
    {
        $this->display();
    }
    public function add()
    {
        $changetime_status = I('changetime_status');
        $data['starttime'] = I('endtime');
        $data['endtime'] = I('starttime');
        $data['started'] = I('started');
        $data['ended'] = I('ended');
        $data['daytime'] = I('daytime');
        $data['worktime'] = I('worktime');
        $data['gocity'] = I('gocity');
        if($data['gocity'] == 1)
        {
            $data['gocity_name'] = I('gocity_name');
        }
        $data['place'] = I('place');
        $data['prices'] = I('price');
        $rs = $this->AceModel->add($data);
        if(false !== $rs || 0 !== $rs){
            $this->success("添加成功");
        }else{
            $this->success("添加失败");
        }
    }
    public function delete()
    {
        $option['id'] = I('id');
        $data['deleted'] = date('Y-m-d H:i:s');
        $rs = $this->AceModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->success("删除成功");
        }else{
            $this->success("删除失败");
        }
    }
    public function save()
    {
        $changetime_status = I('changetime_status');
        $starttime = I('starttime');
        $endtime = I('endtime');
        if($changetime_status == 2 && $starttime != null &&$endtime != null)
        {
            $data['starttime'] = $starttime;
            $data['endtime'] = $endtime;
        }
        $data['started'] = I('started');
        $data['ended'] = I('ended');
        $data['daytime'] = I('daytime');
        $data['worktime'] = I('worktime');
        $data['gocity'] = I('gocity');
        if($data['gocity'] == 1)
        {
            $data['gocity_name'] = I('gocity_name');
        }
        $data['place'] = I('place');
        $data['prices'] = I('price');
        $option['id'] = I('ace_id');
        $rs = $this->AceModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->success("修改成功");
        }else{
            $this->success("修改失败");
        }
    }
    public function addBike()
    {
        $bus_type = I('bus_type');
        $bus_type_num = I('bus_type_num');
        $bus_num = I('bus_num');
        $ace_id = I('ace_id');
        $data['ace_id'] = $ace_id;
        $data['bus_type'] = $bus_type;
        $data['bus_type_num'] = $bus_type_num;
        $data['bus_num'] = $bus_num;
        $rs = $this->busModel->add($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '添加成功'));
        }else{
            $this->ajaxReturn(array('info' => '添加失败'));
        }
    }
    public function busDelete()
    {
        $id = I('id');
        $data['deleted'] = date('Y-m-d H:i:s');
        $option['id'] = $id;
        $rs = $this->busModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '删除成功'));
        }else{
            $this->ajaxReturn(array('info' => '删除失败'));
        }
    }
    public function busChange()
    {
        $id = I('id');
        $data['bus_type'] = I('bus_type');
        $data['bus_type_num'] = I('bus_type_num');
        $data['bus_num'] = I('bus_num');
        $option['id'] = $id;
        $rs = $this->busModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '修改成功'));
        }else{
            $this->ajaxReturn(array('info' => '修改失败'));
        }
    }
    public function addDriver()
    {
        $data['ace_id'] = I('ace_id');
        $data['driver_type'] = I('driver_type');
        $data['driver_lang_type'] = I('driver_lang_type');
        $data['driver_num'] = I('driver_num');
        $rs = $this->driverModel->add($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '添加成功'));
        }else{
            $this->ajaxReturn(array('info' => '添加失败'));
        }
    }
    public function deleteDriver()
    {
        $id = I('id');
        $data['deleted'] = date('Y-m-d H:i:s');
        $option['id'] = $id;
        $rs = $this->driverModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '修改成功'));
        }else{
            $this->ajaxReturn(array('info' => '修改失败'));
        }
    }
    public function changeDriver()
    {
        $option['id'] = I('id');
        $data['driver_lang_type'] = I('driver_lang_type');
        $data['driver_type'] = I('driver_type');
        $data['driver_num'] = I('driver_num');
        $rs = $this->driverModel->where($option)->save($data);
        if(false !== $rs || 0 !== $rs){
            $this->ajaxReturn(array('status' => 'ok', 'info' => '修改成功'));
        }else{
            $this->ajaxReturn(array('info' => '修改失败'));
        }
    }
}
