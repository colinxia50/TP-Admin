<?php
// +----------------------------------------------------------------------
// | TP-Admin [ 多功能后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2016 http://www.hhailuo.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 逍遥·李志亮 <xiaoyao.working@gmail.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

use Admin\Controller\CommonController;

/**
 *
 * 普通会员
 *
 * @author Summer-V
 *        
 */
class GeneralUserController extends CommonController {
	protected $general;
	public function _initialize() {
		$this->general = D ( 'GeneralUser' );
	}
	public function index() {
		$general_info = $this->general->get_general_info ();
		$this->assign ( "info", $general_info );
		$this->display ();
	}
	public function add() {
		if (IS_POST) {
			$this->checkToken ();
			$data = I ( 'post.info' );
			if (!empty($data)) {
				//var_dump($data);
				if($data['pwd']==$data['re_pwd']){
					$save=$this->general->get_save_data($data);
					//var_dump($save);
					if($this->general->add($save)){
						$this->success ( '操作成功！', __MODULE__ . '/GeneralUser/index' );
					}else {
						$this->error ( '操作失败！用户已存在!!', __MODULE__ . '/GeneralUser/index' );
					}
				}else {
					$this->error ( '操作失败！两次密码输入不一致!', __MODULE__ . '/GeneralUser/add' );
				}
				//$this->success ( '操作成功！', __MODULE__ . '/GeneralUser/index' );
			} else {
				$this->error ( '操作失败！', __MODULE__ . '/GeneralUser/index' );
			}
		} else {
            $this->display();
		}
	}
	public function edit() {
		if (IS_POST) {
			$this->checkToken ();
			//var_dump($_POST);
			$datas = $_POST;
			if (! empty ( $datas ['pwd'] )) {
				$datas ['password'] = md5 ( trim ( $datas ['pwd'] ) );
			}
			if($_POST['pwd']!=$_POST['re_pwd']){
				$this->error('两次输入密码不相同！', __MODULE__ . '/GeneralUser/index' );
			}
			$this->general->startTrans ();
			echo "<br>";
			//var_dump($datas);
			$data['id']=$datas['id'];
			$data['g_password']=$datas['password'];
			$data['g_status']=$datas['info']['status'];
			$data['g_update_time']=time();
			//var_dump($data);
			$options['id']=$datas['id'];
			if ($this->general->where($options)->save($data)) {
				$this->general->commit ();
				$this->success ( '操作成功！', __MODULE__ . '/GeneralUser/index' );
			} else {
				$this->general->rollback ();
				$this->error ( '操作失败！', __MODULE__ . '/GeneralUser/index' );
			}
		} else {
			$id = I ( 'get.id' );
			//var_dump($id);
			if (empty ( $id )) {
				$this->error ( '异常操作！', __MODULE__ . '/GeneralUser/index' );
			}
			$option['id']=$id;
			$user = $this->general->where($option)->find();
			//var_dump($user);
			if (empty ( $user )) {
				$this->error ( '会员不存在', __MODULE__ . '/GeneralUser/index' );
			}
			$this->assign ( 'user', $user );
			$this->display ();
		}
	}
	public function del() {
		if(isset($_POST)){
			$this->checkToken ();
			$id = I ( 'get.id' );
			$option['id']=$id;
			if($this->general->where($option)->find()!=null){
				$this->general->where($option)->delete();
				$this->success('操作成功', __MODULE__ . '/GeneralUser/index');
			}else {
				$this->error("会员不存在",__MODULE__.'/GeneralUser/index');
			}
		}else {
			$this->error("非法操作",__MODULE__.'/GeneralUser/index');
		}
	}
	public function search(){
		if(IS_POST){
			if ($_POST['search']=="搜索"){
				$general_info=$this->general->search_info();
				$this->assign ( "info", $general_info );
				$this->display ();
			}else {
				$this->error("非法操作",__MODULE__.'/GeneralUser/index');
			}
		}else {
			$this->error("非法操作",__MODULE__.'/GeneralUser/index');
		}
	}
}